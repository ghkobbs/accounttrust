<?php
	$pagetitle = "AccounTrust Ghana Limited | Accounting Company In Accra, Ghana | Home";
	include("header.php");

?>
	<!--
		=======================
			staticImage
		=======================
	-->
	<div id="staticImage">
		<img src="<?php echo $base_url ?>images/s-about.jpg" alt="About Main Image">
		<div class="staticText clearfix">
			Our Team
			<ul class="breadcrumbs">
				<li><a href="<?php echo $base_url ?>" class="breadcrumbs-links">Accountrust</a></li>
					<span class="breadcrumbs-div"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
				<li>Team</li>
			</ul>
		</div>
		<div id="slider-overlay"></div>
		<div class="url"><?php echo $base_url ?></div>
	</div>
	<!--
		=======================
			staticImage End
		=======================
	-->
	<!--
		=======================
			Team
		=======================
	-->
	<div id="teamPage">
		<div class="container">
			<div class="team-all clearfix">
				<div class="teamItem clearfix">
					<div id="teamItem-Image1" class="teamItem-Image">
						<img src="<?php echo $base_url ?>images/team/10.jpg" alt="Team Image" />
						<div class="Vprofile"><a href="" class="all-links openpro" data-id="1">view profile</a></div>
					</div>
					<div id="teamItem-name1" class="teamItem-name">Maxwell Morrison</div>
					<div id="teamItem-title1" class="teamItem-title">Director</div>
					<ul class="teamItem-socials">
						<li class="teamItem-icons socialicons">
							<a href="http://www.facebook.com/accountrust" target="_blank" id="facebook1" class="all-links"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.twitter.com/accountrust_" target="_blank" id="twitter1" class="all-links"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.instagram.com/accountrust" target="_blank" id="instagram1" class="all-links"><i class="fa fa-instagram" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.linkedin.com/company/accountrust-ghana-limited" target="_blank" id="linkedin1" class="all-links"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
						</li>
					</ul>
					<div id="Bio1" class="Bio">
						Our company is one of the leaders in the market of professional consulting services for medium and large businesses. The peculiarity of our company that distinguishes it from a variety of companies, is the individual integrated approach to solving problems.
					</div>
				</div>
				<div class="teamItem clearfix">
					<div id="teamItem-Image2" class="teamItem-Image">
						<img src="<?php echo $base_url ?>images/team/11.jpg" alt="Team Image" />
						<div class="Vprofile"><a href="" class="all-links openpro" data-id="2">view profile</a></div>
					</div>
					<div id="teamItem-name2" class="teamItem-name">Maxwell Morrison</div>
					<div id="teamItem-title2" class="teamItem-title">Director</div>
					<ul class="teamItem-socials">
						<li class="teamItem-icons socialicons">
							<a href="http://www.facebook.com/accountrust" target="_blank" id="facebook2" class="all-links"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.twitter.com/accountrust_" target="_blank" id="twitter2" class="all-links"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.instagram.com/accountrust" target="_blank" id="instagram2" class="all-links"><i class="fa fa-instagram" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.linkedin.com/company/accountrust-ghana-limited" target="_blank" id="linkedin2" class="all-links"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
						</li>
					</ul>
					<div id="Bio2" class="Bio">
						Our company is one of the leaders in the market of professional consulting services for medium and large businesses. The peculiarity of our company that distinguishes it from a variety of companies, is the individual integrated approach to solving problems.
					</div>
				</div>
				<div class="teamItem clearfix">
					<div id="teamItem-Image3" class="teamItem-Image">
						<img src="<?php echo $base_url ?>images/team/12.jpg" alt="Team Image" />
						<div class="Vprofile"><a href="" class="all-links openpro" data-id="3">view profile</a></div>
					</div>
					<div id="teamItem-name3" class="teamItem-name">Maxwell Morrison</div>
					<div id="teamItem-title3" class="teamItem-title">Director</div>
					<ul class="teamItem-socials">
						<li class="teamItem-icons socialicons">
							<a href="http://www.facebook.com/accountrust" target="_blank" id="facebook3" class="all-links"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.twitter.com/accountrust_" target="_blank" id="twitter3" class="all-links"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.instagram.com/accountrust" target="_blank" id="instagram3" class="all-links"><i class="fa fa-instagram" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.linkedin.com/company/accountrust-ghana-limited" target="_blank" id="linkedin3" class="all-links"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
						</li>
					</ul>
					<div id="Bio3" class="Bio">
						Our company is one of the leaders in the market of professional consulting services for medium and large businesses. The peculiarity of our company that distinguishes it from a variety of companies, is the individual integrated approach to solving problems.
					</div>
				</div>
				<div class="teamItem clearfix">
					<div id="teamItem-Image4" class="teamItem-Image">
						<img src="<?php echo $base_url ?>images/team/13.jpg" alt="Team Image" />
						<div class="Vprofile"><a href="" class="all-links openpro" data-id="4">view profile</a></div>
					</div>
					<div id="teamItem-name4" class="teamItem-name">Maxwell Morrison</div>
					<div id="teamItem-title4" class="teamItem-title">Director</div>
					<ul class="teamItem-socials">
						<li class="teamItem-icons socialicons">
							<a href="http://www.facebook.com/accountrust" target="_blank" id="facebook4" class="all-links"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.twitter.com/accountrust_" target="_blank" id="twitter4" class="all-links"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.instagram.com/accountrust" target="_blank" id="instagram4" class="all-links"><i class="fa fa-instagram" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.linkedin.com/company/accountrust-ghana-limited" target="_blank" id="linkedin4" class="all-links"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
						</li>
					</ul>
					<div id="Bio4" class="Bio">
						Our company is one of the leaders in the market of professional consulting services for medium and large businesses. The peculiarity of our company that distinguishes it from a variety of companies, is the individual integrated approach to solving problems.
					</div>
				</div>
				<div class="teamItem clearfix">
					<div id="teamItem-Image5" class="teamItem-Image">
						<img src="<?php echo $base_url ?>images/team/14.jpg" alt="Team Image" />
						<div class="Vprofile"><a href="" class="all-links openpro" data-id="5">view profile</a></div>
					</div>
					<div id="teamItem-name5" class="teamItem-name">Maxwell Morrison</div>
					<div id="teamItem-title5" class="teamItem-title">Director</div>
					<ul class="teamItem-socials">
						<li class="teamItem-icons socialicons">
							<a href="http://www.facebook.com/accountrust" target="_blank" id="facebook5" class="all-links"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.twitter.com/accountrust_" target="_blank" id="twitter5" class="all-links"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.instagram.com/accountrust" target="_blank" id="instagram5" class="all-links"><i class="fa fa-instagram" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.linkedin.com/company/accountrust-ghana-limited" target="_blank" id="linkedin5" class="all-links"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
						</li>
					</ul>
					<div id="Bio5" class="Bio">
						Our company is one of the leaders in the market of professional consulting services for medium and large businesses. The peculiarity of our company that distinguishes it from a variety of companies, is the individual integrated approach to solving problems.
					</div>
				</div>
				<div class="teamItem clearfix">
					<div id="teamItem-Image6" class="teamItem-Image">
						<img src="<?php echo $base_url ?>images/team/15.jpg" alt="Team Image" />
						<div class="Vprofile"><a href="" class="all-links openpro" data-id="6">view profile</a></div>
					</div>
					<div id="teamItem-name6" class="teamItem-name">Maxwell Morrison</div>
					<div id="teamItem-title6" class="teamItem-title">Director</div>
					<ul class="teamItem-socials">
						<li class="teamItem-icons socialicons">
							<a href="http://www.facebook.com/accountrust" target="_blank" id="facebook6" class="all-links"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.twitter.com/accountrust_" target="_blank" id="twitter6" class="all-links"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.instagram.com/accountrust" target="_blank" id="instagram6" class="all-links"><i class="fa fa-instagram" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.linkedin.com/company/accountrust-ghana-limited" target="_blank" id="linkedin6" class="all-links"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
						</li>
					</ul>
					<div id="Bio6" class="Bio">
						Our company is one of the leaders in the market of professional consulting services for medium and large businesses. The peculiarity of our company that distinguishes it from a variety of companies, is the individual integrated approach to solving problems.
					</div>
				</div>
				<div class="teamItem clearfix">
					<div id="teamItem-Image7" class="teamItem-Image">
						<img src="<?php echo $base_url ?>images/team/16.jpg" alt="Team Image" />
						<div class="Vprofile"><a href="" class="all-links openpro" data-id="7">view profile</a></div>
					</div>
					<div id="teamItem-name7" class="teamItem-name">Maxwell Morrison</div>
					<div id="teamItem-title7" class="teamItem-title">Director</div>
					<ul class="teamItem-socials">
						<li class="teamItem-icons socialicons">
							<a href="http://www.facebook.com/accountrust" target="_blank" id="facebook7" class="all-links"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.twitter.com/accountrust_" target="_blank" id="twitter7" class="all-links"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.instagram.com/accountrust" target="_blank" id="instagram7" class="all-links"><i class="fa fa-instagram" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.linkedin.com/company/accountrust-ghana-limited" target="_blank" id="linkedin7" class="all-links"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
						</li>
					</ul>
					<div id="Bio7" class="Bio">
						Our company is one of the leaders in the market of professional consulting services for medium and large businesses. The peculiarity of our company that distinguishes it from a variety of companies, is the individual integrated approach to solving problems.
					</div>
				</div>
				<div class="teamItem clearfix">
					<div id="teamItem-Image8" class="teamItem-Image">
						<img src="<?php echo $base_url ?>images/team/17.jpg" alt="Team Image" />
						<div class="Vprofile"><a href="" class="all-links openpro" data-id="8">view profile</a></div>
					</div>
					<div id="teamItem-name8" class="teamItem-name">Maxwell Morrison</div>
					<div id="teamItem-title8" class="teamItem-title">Director</div>
					<ul class="teamItem-socials">
						<li class="teamItem-icons socialicons">
							<a href="http://www.facebook.com/accountrust" target="_blank" id="facebook8" class="all-links"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.twitter.com/accountrust_" target="_blank" id="twitter8" class="all-links"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.instagram.com/accountrust" target="_blank" id="instagram8" class="all-links"><i class="fa fa-instagram" aria-hidden="true"></i></a>
						</li>
						<li class="teamItem-icons socialicons">
							<a href="http://www.linkedin.com/company/accountrust-ghana-limited" target="_blank" id="linkedin8" class="all-links"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
						</li>
					</ul>
					<div id="Bio8" class="Bio">
						Our company is one of the leaders in the market of professional consulting services for medium and large businesses. The peculiarity of our company that distinguishes it from a variety of companies, is the individual integrated approach to solving problems.
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="viewPro">
		<div class="closeView">x</div>
		<div class="viewPro-box">
			<div class="viewImage">
				<img src="" alt="Team Image" />
			</div>
			<div class="viewName"></div>
			<div class="viewTitle"></div>
			<div class="viewLabel">Bio</div>
			<div class="viewBio"></div>
			<ul class="teamItem-socials viewSocials">
				<li class="teamItem-icons socialicons">
					<a href="" target="_blank" id="viewFacebook" class="all-links"><i class="fa fa-facebook" aria-hidden="true"></i></a>
				</li>
				<li class="teamItem-icons socialicons">
					<a href="" target="_blank" id="viewTwitter" class="all-links"><i class="fa fa-twitter" aria-hidden="true"></i></a>
				</li>
				<li class="teamItem-icons socialicons">
					<a href="" target="_blank" id="viewInstagram" class="all-links"><i class="fa fa-instagram" aria-hidden="true"></i></a>
				</li>
				<li class="teamItem-icons socialicons">
					<a href="" target="_blank" id="viewLinkedin" class="all-links"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
				</li>
			</ul>
		</div>
	</div>
	<!--
		=======================
			Team
		=======================
	-->
<?php include("footer.php") ?>