<?php
	$pagetitle = "AccounTrust Ghana Limited | Accounting Company In Accra, Ghana | Home";
	include("header.php");

?>
	<!--
		=======================
			staticImage
		=======================
	-->
	<div id="staticImage">
		<img src="<?php echo $base_url ?>images/s-about.jpg" alt="About Main Image">
		<div class="staticText clearfix">
			About us
			<ul class="breadcrumbs">
				<li><a href="<?php echo $base_url ?>" class="breadcrumbs-links">Accountrust</a></li>
					<span class="breadcrumbs-div"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
				<li>About</li>
			</ul>
		</div>
		<div id="slider-overlay"></div>
		<div class="url"><?php echo $base_url ?></div>
	</div>
	<!--
		=======================
			staticImage End
		=======================
	-->
	<!--
		=======================
			About
		=======================
	-->
	<div id="aboutPage">
		<div class="container">
			<div class="about">
				<div class="topSection clearfix">
					<div class="sectionItem sectionText">
						<p class="aboutText">AccounTrust is a limited liability company engaged in the provision of services in the areas of Accounting, Management and Taxation for medium-size enterprises.</p>
						<p class="aboutText">Our primary focus is exceptional client service and expert advice that ensure complete peace of mind for our clients; knowing they are dealing with true professionals who care.</p>
						<p class="aboutText">The goal of AccounTrust is to provide services that will add value to the business of all our clients at all times.</p>
						<p class="aboutText">We have a proven track record for quality work and excellent service in the areas of setting up accounting systems, management contracts for small business, training in accounting and finance for medium size business executives and staff.</p>
						<p class="aboutText">Our clients range from small to medium size businesses, Churches and individual business executives. We provide monthly services for clients in varied industries, including: Legal, Event Organizing, Mining, Real Estate, Schools, Telecommunication Constructions, Poultry Farming, Interior Decorations, Pharmacy etc.</p>
						<p class="aboutText">We have a core specialty in the areas of Accounting systems setups, training in finance, and accounting, advance Microsoft Excel. We are in partnership with practicing Auditing Firms. This gives us a broad range of services and strategies, which allows us to provide extremely satisfactory services to our clients.</p>
					</div>
					<div class="sectionItem sectionImage">
						<img src="<?php echo $base_url ?>images/about1.jpg" alt="About Image">
					</div>
				</div>
			</div>
		</div>
		<div class="midSection clearfix">
			<div class="container">
				<div class="midsection-Item midsection-Text clearfix">
					<p class="aboutText">We commit ourselves to providing professional and value driven services to our clients at all times. To achieve this we maintain a highly motivated work force with an orientation towards the achievement of excellence in all areas of our corporate endeavours to ensure the ultimate satisfaction of our clients.</p>
					<p class="aboutText">We provide solutions to the Business, Finance, Accounting and Taxation challenges of small and medium size enterprises.</p>
				</div>
				<ul class="midsection-list midsection-Text clearfix">
					<li class="midsection-listItem">
						Our business is defined by the needs of our clients and as such our client’s best interests always come first.
					</li>
					<li class="midsection-listItem">
						Our goal is to provide services that will add value to the business of all our clients at all times.
					</li>
					<li class="midsection-listItem">
						We maintain confidentiality with clients’ business information and trade secrets.
					</li>
				</ul>
			</div>
			<div id="midsection-overlay"></div>
		</div>
	</div>
	<!--
		=======================
			About End
		=======================
	-->
	<!--
		=======================
			Clients
		=======================
	-->
	<div id="clients">
		<div class="container clearfix">
			<div class="allGroup clientGroup">
				<div class="mainTitle"><h1>Clients</h1></div>
				<div class="clientsItems clearfix">
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client1.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client2.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client3.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client4.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client5.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client6.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client7.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client8.png" alt="Clients"></div>
					</div>
				</div>
				<div class="clientbtn">
					<a href="<?php echo $base_url ?>clients" class="sub-btn main-btn">View more clients</a>
				</div>
			</div>
		</div>
	</div>
	<!--
		=======================
			Clients End
		=======================
	-->
<?php include("footer.php") ?>