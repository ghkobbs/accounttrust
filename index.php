<?php
	$pagetitle = "AccounTrust Ghana Limited | Accounting Company In Accra, Ghana | Home";
	include("header.php");
?>	
	<!--
		=======================
			Slider
		=======================
	-->
	<div id="slider">
		<div id="owl-demo" class="owl-carousel owl-theme">
			<div class="item"><img src="<?php echo $base_url ?>images/slider/image.jpg" alt=""></div>
			<div class="item"><img src="<?php echo $base_url ?>images/slider/image.jpg" alt=""></div>
			<div class="item"><img src="<?php echo $base_url ?>images/slider/image.jpg" alt=""></div>
		</div>
		<div class="megaText">
			WE PROVIDE ADVICE WHEN YOUR BUSINESS NEEDS IT, NOT JUST WHEN YOU ASK FOR IT!
			<p class="mega-smallText">OUR COMPANY PROVIDES HIGH QUALITY PROFESSIONAL ACCOUNTING SERVICES FOR ENTERPRISES AND PRIVATE ENTREPRENEURS.</p>
			<div class="call-to-action">
				<a href="<?php echo $base_url ?>about" class="btn mega-btn">Learn more</a>
				<a href="" class="btn mega-btn bookapt">Book appointment</a>
			</div>
		</div>
		<div id="sliderFooter" class="clearfix">
			<div class="sliderFooter-item">
				<div class="sliderFooter-info clearfix">
					<div class="topIcons sliderFooter-icon"><i class="fa fa-cubes" aria-hidden="true"></i></div>
					<div class="sliderFooter-info-tile">Effective</div>
					<div class="sliderFooter-info-sub-title">EFFECTIVE MANAGEMENT</div>
				</div>	
			</div>
			<div class="sliderFooter-item">
				<div class="sliderFooter-info clearfix">
					<div class="topIcons sliderFooter-icon"><i class="fa fa-dot-circle-o" aria-hidden="true"></i></div>
					<div class="sliderFooter-info-tile">Client Focus</div>
					<div class="sliderFooter-info-sub-title">INDIVIDUAL APPROACH</div>
				</div>	
			</div>
			<div class="sliderFooter-item">
				<div class="sliderFooter-info clearfix">
					<div class="topIcons sliderFooter-icon"><i class="fa fa-trophy" aria-hidden="true"></i></div>
					<div class="sliderFooter-info-tile">Be The Leader</div>
					<div class="sliderFooter-info-sub-title">HIGH STANDARDS</div>
				</div>	
			</div>
		</div>
		<div id="slider-overlay"></div>
		<div class="url"><?php echo $base_url ?></div>
	</div>
	<!--
		=======================
			Slider End
		=======================
	-->
	<!--
		=======================
		Show on mobile device
		=======================
	-->
		<div id="mo-sliderFooter" class="clearfix">
			<div class="sliderFooter-item">
				<div class="sliderFooter-info clearfix">
					<div class="topIcons sliderFooter-icon"><i class="fa fa-cubes" aria-hidden="true"></i></div>
					<div class="sliderFooter-info-tile">Effective</div>
					<div class="sliderFooter-info-sub-title">EFFECTIVE MANAGEMENT</div>
				</div>	
			</div>
			<div class="sliderFooter-item">
				<div class="sliderFooter-info clearfix">
					<div class="topIcons sliderFooter-icon"><i class="fa fa-dot-circle-o" aria-hidden="true"></i></div>
					<div class="sliderFooter-info-tile">Client Focus</div>
					<div class="sliderFooter-info-sub-title">INDIVIDUAL APPROACH</div>
				</div>	
			</div>
			<div class="sliderFooter-item">
				<div class="sliderFooter-info clearfix">
					<div class="topIcons sliderFooter-icon"><i class="fa fa-trophy" aria-hidden="true"></i></div>
					<div class="sliderFooter-info-tile">Be The Leader</div>
					<div class="sliderFooter-info-sub-title">HIGH STANDARDS</div>
				</div>	
			</div>
		</div>
	<!--
		=======================
		Show on mobile device
		=======================
	-->

	<!--
		=======================
			Appointment Sec
		=======================
	-->
	<div id="apt-section">
		<div class="aptclose">x</div>
		<div class="aptSec-Group">
			<div class="aptHead"><h2>Schedule Appointment</h2></div>
			<form id="appointmentForm" method="POST" action="">
				<div id="step1">
					<div class="error nerror"></div>
					<div class="input-box clearfix">
						<input type="text" class="name apt-inputField" name="name" placeholder="Name *">
						<div class="error aptnameerror"></div>
					</div>
					<div class="input-box clearfix">
						<input type="text" class="email apt-inputField" name="email" placeholder="Email *">
						<div class="error aptemailerror"></div>
					</div>
					<div class="input-box clearfix">
						<input type="text" class="phone apt-inputField" name="phone" placeholder="Phone *">
						<div class="error aptphoneerror"></div>
					</div>
					<div class="input-box clearfix">
						<input type="text" class="company apt-inputField" name="company" placeholder="Company">
						<div class="error aptcompanyerror"></div>
					</div>
					<div class="input-box clearfix">
						<textarea class="message apt-inputField-large" name="message" placeholder="Message *" ></textarea>
						<div class="error aptmessageerror"></div>
					</div>
					<div class="apt-subHead"><h3>Date of Appointment</h3></div>
					<div class="divcalendar">

						<div id="calendaroverallcontrols">
						  <!-- <div id="year"></div> -->

						  <div id="calendarmonthcontrols">
						    <!--<a id="btnPrevYr" href="#" title="Previous Year"><span><<</span></a>

						    <a id="btnPrev" href="#" title="Previous Month"><span><</span></a>-->

						    <!-- <input type="button" src="images/btnprevmonth.png" alt="Submit" id="btnPrev"/>-->

						    <!-- <div id="month"></div>-->

						    <div id="monthandyear"></div>

						    <!--<input type="button" src="images/btnnextmonth.png" alt="Submit" id="btnNext"/>-->

						    <!--<a id="btnNext" href="#" title="Next Month"><span>></span></a>

						    <a id="btnNextYr" href="#" title="Next Year"><span>>></span></a>-->  
						  </div>
						</div>

						<div id="divcalendartable"></div>

						<div class="aptLabel">Appointment Date</div>
						<div class="aptDate-col clearfix">
							<div class="selDate">click on a date from the calender</div>
							<div id="newmonthandyear" class="aptDate"></div>
						</div>
						<div class="error aptdateerror"></div>
						<div class="aptReset">Reset Date</div>
					</div>
					<div class="apt-btn step1"><i class="fa fa-arrow-right"></i></div>
				</div>
				<div id="step2">
					<div class="loadcon">
						<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
						<div class="">Please wait...</div>
					</div>
				</div>
				<div id="step3">
					<div class="aptHead"><h3>Confirm Details</h3></div>
					<div class="Det-box">
						<div class="detLabel aptLabel">Apointment Date</div>
						<div class="det-date aptDet">20 January, 2015</div>
					</div>
					<div class="Det-box">
						<div class="detLabel aptLabel">Name</div>
						<div class="det-name aptDet">Maxwell Morrison</div>
					</div>
					<div class="Det-box">
						<div class="detLabel aptLabel">Email</div>
						<div class="det-email aptDet">ghkobbs@box.com</div>
					</div>
					<div class="Det-box">
						<div class="detLabel aptLabel">Phone</div>
						<div class="det-phone aptDet">0541906742</div>
					</div>
					<div class="Det-box">
						<div class="detLabel aptLabel">Company</div>
						<div class="det-company aptDet"></div>
					</div>
					<div class="Det-box">
						<div class="detLabel aptLabel">Message</div>
						<div class="det-message aptDet">Our company is one of the leaders in the market of professional consulting services for medium and large businesses. The peculiarity of our company that distinguishes it from a variety of companies, is the individual integrated approach to solving problems. The specifics of consulting services in the</div>
					</div>
					<div class="error uerror"></div>
					<button type="submit" class="apt-btn"><i class="fa fa-arrow-right"></i></button>
				</div>
			</form>
			<div class="aptsuccess"></div>
		</div>
	</div>
	<!--
		=======================
		  Appointment Sec End
		=======================
	-->
	<!--
		=======================
			Intro
		=======================
	-->
	<div id="intro">
		<div class="container clearfix">
			<div class="introText">
				<div class="mainTitle introTitle"><h1>Welcome to Our Site</h1></div>
				<div class="intro-pText">
					Our company is one of the leaders in the market of professional consulting services for medium and large businesses. The peculiarity of our company that distinguishes it from a variety of companies, is the individual integrated approach to solving problems. The specifics of consulting services in the area of improvement of business efficiency is that it is impossible to solve the problem within the framework of only one type of consulting
				</div>
				<a href="<?php echo $base_url ?>about" class="btn main-btn">Learn more</a>
			</div>
		</div>
	</div>
	<!--
		=======================
			Intro End
		=======================
	-->
	<!--
		=======================
			Services
		=======================
	-->
	<div id="services">
		<div class="container clearfix">
			<div class="allGroup ServiceGroup">
				<div class="mainTitle introTitle"><h1>Services</h1></div>
				<div class="serviceItem-Group clearfix">
					<div class="serviceItem">
						<div class="MainIcon serviceIcon"><i class="fa fa-calculator" aria-hidden="true"></i></div>
						<div class="serviceTitle">ACCOUNTING SERVICES</div>
						<div class="serviceDesc">
							We are able to establish accounting and managerial accounting of major businesses and company
						</div>
					</div>
					<div class="serviceItem">
						<div class="MainIcon serviceIcon"><i class="fa fa-book" aria-hidden="true"></i></div>
						<div class="serviceTitle">BOOKKEEPING</div>
						<div class="serviceDesc">
							We take responsibility for the conduct  and recovery of accounting and tax record.
						</div>
					</div>
					<div class="serviceItem">
						<div class="MainIcon serviceIcon"><i class="fa fa-microchip" aria-hidden="true"></i></div>
						<div class="serviceTitle">ACCOUNTING SOFTWARE</div>
						<div class="serviceDesc">
							Our company provides the best accounting software solutions for companies and businesses.
						</div>
					</div>
				</div>
				<a href="<?php echo $base_url ?>services" class="btn main-btn">View more</a>
			</div>
		</div>
	</div>
	<!--
		=======================
			Services End
		=======================
	-->
	<!--
		=======================
			whyUs
		=======================
	-->
	<div id="whyUs">
		<div class="container clearfix">
			<div class="allGroup whyusGroup">
				<div class="mainTitle showline"><h1>Our Advantages</h1></div>
				<div class="whyus-itemGroup clearfix">
					<div class="whyusItem firstItem">
						<div class="whyusIcon"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></div>
						<div class="whyusTitle">Advise on professional polutions</div>
						<div class="whyusDesc">The realizability of our solutions is provided by mandatory training of personnel for the introduction of the client company, visiable and complete documentation of all procedures.</div>
					</div>
					<div class="whyusItem secondItem">
						<div class="whyusIcon"><i class="fa fa-life-ring" aria-hidden="true"></i></div>
						<div class="whyusTitle">Keep Our Clients Abreast of Changes in the Law</div>
						<div class="whyusDesc">We'll restrict you from mistakes and will always give you tips, that will help you avoid violations of the law. Our company is one of the leaders in the market of professional consulting services.</div>
					</div>
				</div>
			</div>
			<div class="allGroup whyusGroup">
				<div class="mainTitle"><h1>Our Capabilities</h1></div>
				<div class="whyus-itemGroup clearfix">
					<div class="whyusItem thirdItem">
						<div class="whyusIcon"><i class="fa fa-lightbulb-o" aria-hidden="true"></i></div>
						<div class="whyusTitle">WE KNOW HOW</div>
						<div class="whyusDesc">We offer you business advice that you can get from us at any convenient time for you.</div>
					</div>
					<div class="whyusItem fourthItem">
						<div class="whyusIcon"><i class="fa fa-line-chart" aria-hidden="true"></i></div>
						<div class="whyusTitle">BUSINESS GROWTH</div>
						<div class="whyusDesc">You will get a reliable partner, focused on providing high indicators for your project.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--
		=======================
			whyUs End
		=======================
	-->
	<!--
		=======================
			Blog
		=======================
	-->
	<div id="blog">
		<div class="container clearfix">
			<div class="allGroup blogGroup">
				<div class="mainTitle"><h1>Recent Posts</h1></div>
				<div class="blog-itemGroup clearfix">
					<?php 
						$check = mysqli_query($db_connect, "SELECT * FROM blog ORDER BY blog_id DESC LIMIT 3");
						while($get = mysqli_fetch_assoc($check)){
							$blog_image = $get['blog_image'];
							$blog_title = $get['blog_title'];
							$blog_url = $get['blog_url'];
							$blog_date = $get['blog_date'];
							$blog_authour = $get['blog_authour'];
							$blog_content = $get['blog_content'];

							$blog_date = date("jS M, Y / H:i:a", strtotime($blog_date));
							$blog_authour_url = str_replace(' ','-',$blog_authour);

							if($blog_url == ""){

								$blog_url = str_replace(' ','-',$blog_title);

							} else {
								$blog_url = str_replace(' ','-',$blog_url);
							}

							echo '


								<div class="blogItem">
									<div class="blogTitle"><h2><a href="'.$base_url.'blog/article/'.$blog_url.'" class="blog-links">'.$blog_title.'</a></h2></div>
									<div class="blogInfo clearfix">
										<div class="blogInfo-head"><span class="blogInfo-title">By: </span>'.$blog_authour.'</div>
										<div class="blogInfo-head"><span class="blogInfo-title">Posted on: </span>'.$blog_date.'</div>
									</div>
									<div class="blogImage">
										<img src="'.$base_url.'images/blog/'.$blog_image.'" />
									</div>
									<div class="blogContent">'.$blog_content.'</div>
									<a href="'.$base_url.'blog/article/'.$blog_url.'" class="btn main-btn">Read more</a>
								</div>


							';

						}

					?>
					<div class="homeBlog-btn clearfix">
						<a href="<?php echo $base_url ?>blog" class="sub-btn main-btn">View all posts</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--
		=======================
			Blog End
		=======================
	-->
	<!--
		=======================
			Testimonies
		=======================
	-->
	<div id="Testmonials">
		<div class="container clearfix">
			<div class="allGroup testGroup">
				<div class="mainTitle"><h1>What our clients are saying	</h1></div>
				<div class="test-box clearfix">
					<div id="owl-testimonials" class="owl-carousel owl-theme">
						<div class="item">
							<div class="test-item">
								<div class="testImage">
									<img src="<?php echo $base_url ?>images/testimonials/10.jpg" alt="">
								</div>
								<div class="testContent">
									What a great company. Dan in the office helped me get me claim filed and the tech Jason was very friendly and helpful as well. he did a great job on my windshield and even vacuumed out the front of my truck. Great experience from start to finish. will recommend them to all my friends.
								</div>
								<div class="testDivider"></div>
								<div class="testInfo">
									<p>Maxwell Morrison</p>
									<p>Web Developer</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="test-item">
								<div class="testImage">
									<img src="<?php echo $base_url ?>images/testimonials/11.jpg" alt="">
								</div>
								<div class="testContent">
									What a great company. Dan in the office helped me get me claim filed and the tech Jason was very friendly and helpful as well. he did a great job on my windshield and even vacuumed out the front of my truck. Great experience from start to finish. will recommend them to all my friends.
								</div>
								<div class="testDivider"></div>
								<div class="testInfo">
									<p>Maxwell Morrison</p>
									<p>Web Developer</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="test-item">
								<div class="testImage">
									<img src="<?php echo $base_url ?>images/testimonials/12.jpg" alt="">
								</div>
								<div class="testContent">
									What a great company. Dan in the office helped me get me claim filed and the tech Jason was very friendly and helpful as well. he did a great job on my windshield and even vacuumed out the front of my truck. Great experience from start to finish. will recommend them to all my friends.
								</div>
								<div class="testDivider"></div>
								<div class="testInfo">
									<p>Maxwell Morrison</p>
									<p>Web Developer</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="test-item">
								<div class="testImage">
									<img src="<?php echo $base_url ?>images/testimonials/13.jpg" alt="">
								</div>
								<div class="testContent">
									What a great company. Dan in the office helped me get me claim filed and the tech Jason was very friendly and helpful as well. he did a great job on my windshield and even vacuumed out the front of my truck. Great experience from start to finish. will recommend them to all my friends.
								</div>
								<div class="testDivider"></div>
								<div class="testInfo">
									<p>Maxwell Morrison</p>
									<p>Web Developer</p>
								</div>
							</div>
						</div>
					</div>
					<div class="customNavigation">
						<a class="test-btn prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
						<a class="test-btn next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--
		=======================
			Testimonies
		=======================
	-->
	<!--
		=======================
			Clients
		=======================
	-->
	<div id="clients">
		<div class="container clearfix">
			<div class="allGroup clientGroup">
				<div class="mainTitle"><h1>Clients</h1></div>
				<div class="clientsItems clearfix">
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client1.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client2.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client3.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client4.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client5.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client6.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client7.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client8.png" alt="Clients"></div>
					</div>
				</div>
				<div class="clientbtn">
					<a href="<?php echo $base_url ?>clients" class="sub-btn main-btn">View more clients</a>
				</div>
			</div>
		</div>
	</div>
	<!--
		=======================
			Clients End
		=======================
	-->
<script type="text/javascript" src="<?php echo $base_url ?>js/clndr.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>js/owljs.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>js/owl.carousel.js"></script>
<!--<script type="text/javascript" src="<?php echo $base_url ?>js/owl.carousel.min.js"></script>-->
<?php include("footer.php") ?>