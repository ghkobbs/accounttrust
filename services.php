<?php
	$pagetitle = "AccounTrust Ghana Limited | Accounting Company In Accra, Ghana | Home";
	include("header.php");

?>
	<!--
		=======================
			staticImage
		=======================
	-->
	<div id="staticImage">
		<img src="<?php echo $base_url ?>images/s-about.jpg" alt="About Main Image">
		<div class="staticText clearfix">
			Our Services
			<ul class="breadcrumbs">
				<li><a href="<?php echo $base_url ?>" class="breadcrumbs-links">Accountrust</a></li>
					<span class="breadcrumbs-div"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
				<li>Services</li>
			</ul>
		</div>
		<div id="slider-overlay"></div>
		<div class="url"><?php echo $base_url ?></div>
	</div>
	<!--
		=======================
			staticImage End
		=======================
	-->
	<!--
		=======================
			Services
		=======================
	-->
	<div id="servicePage">
		<div class="container">
			<div class="services">
				<div class="servicesItem clearfix">
					<div class="servicesIcon">
						<img src="<?php echo $base_url ?>images/act-icon.png" alt="Accountancy icon">
					</div>
					<div class="serviceInfo-box">
						<div class="servicesName">Accounting</div>
						<div class="servicesDesc">
							We Maintain proper books of accounts for SMEs and NGOs
							We provide monthly management performance and improvement report. Our reports give general observations and performance improvement recommendations.
						</div>
					</div>
				</div>
				<div class="servicesItem clearfix">
					<div class="servicesIcon">
						<img src="<?php echo $base_url ?>images/org-icon.png" alt="Accountancy icon">
					</div>
					<div class="serviceInfo-box">
						<div class="servicesName">Organizational Development</div>
						<div class="servicesDesc">
							We are an Organizational Development (OD) company specifically capacity building in Human Resource and Finance and Accounting. Building organizational capacity is crucial for NGOs and SME to ensure they achieve their mission and for long term sustainability.
						</div>
					</div>
				</div>
				<div class="servicesItem clearfix">
					<div class="servicesIcon">
						<img src="<?php echo $base_url ?>images/fin-icon.png" alt="Accountancy icon">
					</div>
					<div class="serviceInfo-box">
						<div class="servicesName">Finance And Accounting Capacity</div>
						<div class="servicesDesc">
							Provide training in proper book keeping for NGOs and SMEs. Review of Accounting systems and overall business structures and recommend improvement. Provide budgetary and monitoring services. Assist in the implementation of accounting systems including software acquisition and installation
						</div>
					</div>
				</div>
				<div class="servicesItem clearfix">
					<div class="servicesIcon">
						<img src="<?php echo $base_url ?>images/man-icon.png" alt="Accountancy icon">
					</div>
					<div class="serviceInfo-box">
						<div class="servicesName">Management Contract</div>
						<div class="servicesDesc">
							All aspects of running of the company on behalf of the business owner. We also provide total business solutions for all kinds of organizations
						</div>
					</div>
				</div>
				<div class="servicesItem clearfix">
					<div class="servicesIcon">
						<img src="<?php echo $base_url ?>images/bus-icon.png" alt="Accountancy icon">
					</div>
					<div class="serviceInfo-box">
						<div class="servicesName">Business Advisory</div>
						<div class="servicesDesc">
							Engaging the services of professionals and legally recognized organizations provide a lot of relief in this direction. With our experience and knowledge gained from years of working with diverse businesses we are always in the position to provide good operational management for these viable SMEs to ensure growth and sustainability.
						</div>
					</div>
				</div>
				<div class="servicesItem clearfix">
					<div class="servicesIcon">
						<img src="<?php echo $base_url ?>images/con-icon.png" alt="Accountancy icon">
					</div>
					<div class="serviceInfo-box">
						<div class="servicesName">Management Consultancy</div>
						<div class="servicesDesc">
							Assist in the provision of appropriate documentation for acquiring facilities from the banks. Preparation of business plans and proposals. Preparation of company profile. Preparation of company capacity statements
						</div>
					</div>
				</div>
				<div class="servicesItem clearfix">
					<div class="servicesIcon">
						<img src="<?php echo $base_url ?>images/ups-icon.png" alt="Accountancy icon">
					</div>
					<div class="serviceInfo-box">
						<div class="servicesName">Business Start - Ups</div>
						<div class="servicesDesc">
							From inception to completion, we help in Registering your company, Setting up office, System set ups, Staff recruitment among others
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--
		=======================
			Services End
		=======================
	-->
	<!--
		=======================
			Clients
		=======================
	-->
	<div id="clients">
		<div class="container clearfix">
			<div class="allGroup clientGroup">
				<div class="mainTitle"><h1>Clients</h1></div>
				<div class="clientsItems clearfix">
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client1.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client2.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client3.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client4.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client5.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client6.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client7.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client8.png" alt="Clients"></div>
					</div>
				</div>
				<div class="clientbtn">
					<a href="<?php echo $base_url ?>clients" class="sub-btn main-btn">View more clients</a>
				</div>
			</div>
		</div>
	</div>
	<!--
		=======================
			Clients End
		=======================
	-->
<?php include("footer.php") ?>