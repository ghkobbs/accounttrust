<?php
	ob_start();
	$pagetitle = "AccounTrust Ghana Limited | Accounting Company In Accra, Ghana | Home";
	include("header.php");

	
	if(isset($_POST['submit'])){
		$name = mysqli_real_escape_string($db_connect, $_POST['name']);
		$email = mysqli_real_escape_string($db_connect, $_POST['email']);
		$phone = mysqli_real_escape_string($db_connect, $_POST['phone']);
		$subject = mysqli_real_escape_string($db_connect, $_POST['subject']);
		$message = mysqli_real_escape_string($db_connect, $_POST['message']);

		if($message != ""){
		
			$insmsg = mysqli_query($db_connect, "INSERT INTO `messages` (`id`, `name`, `email`, `phone`, `subject`, `message`) VALUES (NULL, '$name', '$email', '$phone', '$subject', '$message') ");
		}
		ob_end_clean();

		if($insmsg){
			require 'phpmailer/PHPMailerAutoload.php';

			$mail = new PHPMailer;

			//$mail->SMTPDebug = 3;                               // Enable verbose debug output

			$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = 'bluewavestechsmtp@gmail.com';                 // SMTP username
			$mail->Password = 'demonstration2016';                           // SMTP password
			$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 587;                                    // TCP port to connect to

			$mail->setFrom($email, $name);
			$mail->addAddress('mmorrison@webteklimited.com', 'Maxwell Morrison');     // Add a recipient
			$mail->addReplyTo($email, $name);
			//$mail->addCC('cc@example.com');
			//$mail->addBCC('bcc@example.com');
			$mail->isHTML(true);                                  // Set email format to HTML

			$mail->Subject = "hello";
			$mail->Body    = '<html><body>';
			$mail->Body    .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
			$mail->Body    .= "<tr><td><strong>Name:</strong> </td><td>" . $name . "</td></tr>";
			$mail->Body    .= "<tr><td><strong>Email:</strong> </td><td>" .$email . "</td></tr>";
			$mail->Body    .= "<tr><td><strong>Contact:</strong> </td><td>" .$phone . "</td></tr>";
			$mail->Body    .= "<tr><td><strong>Message:</strong> </td><td>" .$message . "</td></tr>";
			$mail->Body    .= "</table>";
			$mail->Body    .= '</body></html>';
			$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			if(!$mail->send()) {
			    echo json_encode(array("messageResponse" => $mail->ErrorInfo));
				exit();
			} else {
			    echo json_encode(array("messageResponse" => "added"));
				exit();
			}
		}
	}	

?>	
	<!--
		=======================
			HeaderTop
		=======================
	-->
	<div id="headerTop">
		<div class="headerItems-group clearfix">
			<div class="topBox">
				<div class="topItem">
					<span class="topIcons topItem-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
					<a href="<?php echo $base_url ?>contact#map" class="all-links topItem-link">Feo Oyeo Link, North Industrial Area, Accra-Ghana</a>
				</div>
			</div>
			<div class="topBox">
				<div class="topItem">
					<span class="topIcons topItem-icon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
					Monday - Friday: 08:00am - 05:00pm
				</div>
			</div>
			<div class="topBox">
				<div class="topItem">
					<span class="topIcons topItem-icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
					<a href="tel:+233 30 226 5323" class="all-links">+233 30 226 5323</a> / <a href="tel:+233 20 636 7332" class="all-links">+233 20 636 7332</a>
				</div>
			</div>
		</div>
	</div>
	<!--
		=======================
			HeaderTop
		=======================
	-->
	<!--
		=======================
			Header
		=======================
	-->
	<div id="header">
		<div class="headerItems-group clearfix">
			<div id="logo">
				<img src="<?php echo $base_url ?>images/logo.png" alt="Logo"/>
			</div>
			<div class="show-on-scroll clearfix">
				<div class="topBox">
					<div class="topItem">
						<span class="topIcons topItem-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
						<a href="<?php echo $base_url ?>contact#map" class="all-links topItem-link">Feo Oyeo Link, North Industrial Area, Accra-Ghana</a>
					</div>
				</div>
				<div class="topBox">
					<div class="topItem">
						<span class="topIcons topItem-icon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
						Monday - Friday: 08:00am - 05:00pm
					</div>
				</div>
				<div class="topBox">
					<div class="topItem">
						<span class="topIcons topItem-icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
						<a href="tel:+233 30 226 5323" class="all-links">+233 30 226 5323</a> / <a href="tel:+233 20 636 7332" class="all-links">+233 20 636 7332</a>
					</div>
				</div>
			</div>
			<div id="menu">
				<span class="openMenu"><i class="fa fa-bars" aria-hidden="true"></i></span>
			</div>
			<div id="search">
				<span class="openSearch"><i class="fa fa-search" aria-hidden="true"></i></span>
			</div>
			<div class="popMenu fixed">
				<span class="close-popMenu">x</span>
				<ul class="nav">
					<li class="nav-item"><a href="<?php echo $base_url ?>" class="all-links">Home</a></li>
					<li class="nav-item"><a href="<?php echo $base_url ?>about" class="all-links">About</a></li>
					<li class="nav-item"><a href="<?php echo $base_url ?>services" class="all-links">Services</a></li>
					<li class="nav-item"><a href="<?php echo $base_url ?>team" class="all-links">Team</a></li>
					<li class="nav-item"><a href="<?php echo $base_url ?>contact" class="all-links current-item">Contact</a></li>
					<li class="nav-item"><a href="<?php echo $base_url ?>clients" class="all-links">Clients</a></li>
				</ul>
			</div>
			<div class="popSearch fixed">
				<span class="close-popSearch">x</span>
				<form method="POST" action="">
					<input type="text" class="searchInput" name="searchInput" placeholder="Search site..."/>
				</form>
				<ul class="searchResults">
					<li class="searchItem">
						<a href="" class="all-links">
							<p class="resultsTitle">Thank you father Lord</p>
							<p class="resultsDate">posted on 23rd Dec, 2016</p>
						</a>
					</li>
					<li class="searchItem">
						<a href="" class="all-links">
							<p class="resultsTitle">Thank you father Lord</p>
							<p class="resultsDate">posted on 23rd Dec, 2016</p>
						</a>
					</li>
					<li class="searchItem">
						<a href="" class="all-links">
							<p class="resultsTitle">Thank you father Lord</p>
							<p class="resultsDate">posted on 23rd Dec, 2016</p>
						</a>
					</li>
					<li class="searchItem">
						<a href="" class="all-links">
							<p class="resultsTitle">Thank you father Lord</p>
							<p class="resultsDate">posted on 23rd Dec, 2016</p>
						</a>
					</li>
					<li class="searchItem">
						<a href="" class="all-links">
							<p class="resultsTitle">Thank you father Lord</p>
							<p class="resultsDate">posted on 23rd Dec, 2016</p>
						</a>
					</li>
					<li class="searchItem">
						<a href="" class="all-links">
							<p class="resultsTitle">Thank you father Lord</p>
							<p class="resultsDate">posted on 23rd Dec, 2016</p>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!--
		=======================
			Header
		=======================
	-->
	<!--
		=======================
			staticImage
		=======================
	-->
	<div id="staticImage">
		<img src="<?php echo $base_url ?>images/s-about.jpg" alt="About Main Image">
		<div class="staticText clearfix">
			Get in touch
			<ul class="breadcrumbs">
				<li><a href="<?php echo $base_url ?>" class="breadcrumbs-links">Accountrust</a></li>
					<span class="breadcrumbs-div"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
				<li>Contact</li>
			</ul>
		</div>
		<div id="slider-overlay"></div>
	</div>
	<!--
		=======================
			staticImage End
		=======================
	-->
	<!--
		=======================
			Contact
		=======================
	-->
	<!--
		=======================
			Contact
		=======================
	-->

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAbBZGARIZvM6MnquA-oq94xHBpxFnrPRo&callback=initMap&libraries=places"
    async defer></script>
<?php include("footer.php") ?>