<?php 
include("db_connect.php");

if($pagetitle == ""){
	$pagetitle = "AccounTrust Ghana Limited | Accounting Company In Accra, Ghana | Home";
} else {
	$pagetitle = $pagetitle;
}

?>
<!DOCTYPE html>
<html  class="no-js" lang="en">
<head>
	<title><?php echo $pagetitle ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="AccounTrust Ghana Limited is an Accounting Company In Accra, Ghana. Looking for a full Accounting Company Company? You came to the right place &ndash; learn more now!">
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta http-equiv="etag" content="5fc27cec8b90748f50dc1de63810f9e9"/>
	<meta property="og:title" content="<?php echo $pagetitle ?>"/>
	<meta property="og:type" content="website"/>
	<meta property="og:url" content="<?php echo $base_url ?>"/>
	<meta property="og:image" content="<?php echo $base_url ?>images/sm-logo.png"/>
	<meta property="fb:admins" content="accountrust"/>
	<meta property="og:description" content="AccounTrust Ghana Limited is an Accounting Company In Accra, Ghana. Looking for a full Accounting Company Company? You came to the right place &ndash; learn more now!"/>
	<meta name="twitter:card" content="summary">
	<meta name="twitter:url" content="<?php echo $base_url ?>">
	<meta name="twitter:title" content="<?php echo $pagetitle ?>">
	<meta name="twitter:image" content="<?php echo $base_url ?>/images/sm-logo.png">
	<meta name="twitter:description" content="AccounTrust Ghana Limited is an Accounting Company In Accra, Ghana. Looking for a full Accounting Company Company? You came to the right place &ndash; learn more now!">
	<meta name="keywords" content="Accounting Companies in Accra, Ghana, Management Companies in Accr"/>
	<link rel="publisher" href="https://plus.google.com/+Accountrustltd"/>
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>css/owl.carousel.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>css/owl.theme.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>css/owl.transitions.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>css/style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>css/font-awesome.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>css/font-awesome.min.css" />
	<script type="text/javascript" src="<?php echo $base_url ?>js/jquery.js"></script>
	<script type="text/javascript">

			if ( $(window).width() < 709+"px") { 

				//Make navigation fixed
				$(window).scroll(function () {
				    //if you hard code, then use console
				    //.log to determine when you want the 
				    //nav bar to stick.  
					if ($(window).scrollTop() > 0) {
						$("#header").addClass("fixed");
						$("#header").css("margin-top","0");
						$("#header").css("background","#fff");
						$("#header").css("box-shadow","0 0 4px 0 rgba(0,0,0,.08), 0 2px 4px 0 rgba(0,0,0,.12)");
						$(".show-on-scroll").css("margin-top","0");
						$("#menu").css("color","#00bff3");
						$("#search").css("color","#00bff3");
					  	$("#logo").html('<a href="<?php echo $base_url ?>"><img src="<?php echo $base_url ?>images/scrolllogo.png" alt="Logo"/></a>')
					}
					if ($(window).scrollTop() > 709) {
						$("#header").removeClass("fixed");
						$("#header").css("margin-top","0px");
						$("#header").css("background","transparent");
						$("#header").css("box-shadow","none");
						$(".show-on-scroll").css("margin-top","-113px");
						$("#menu").css("color","#fff");
						$("#search").css("color","#fff");
					  	$("#logo").html('<a href="<?php echo $base_url ?>"><img src="<?php echo $base_url ?>images/logo.png" alt="Logo"/></a>')				 
					}
				});     
			 	
			} else {

				//Make navigation fixed
				$(window).scroll(function () {
				    //if you hard code, then use console
				    //.log to determine when you want the 
				    //nav bar to stick.  
					if ($(window).scrollTop() > 49) {
						$("#header").addClass("fixed");
						$("#header").css("margin-top","0");
						$("#header").css("background","#fff");
						$("#header").css("box-shadow","0 0 4px 0 rgba(0,0,0,.08), 0 2px 4px 0 rgba(0,0,0,.12)");
						$(".show-on-scroll").css("margin-top","0");
						$("#menu").css("color","#00bff3");
						$("#search").css("color","#00bff3");
					  	$("#logo").html('<a href="<?php echo $base_url ?>"><img src="<?php echo $base_url ?>images/scrolllogo.png" alt="Logo"/></a>')
					}
					if ($(window).scrollTop() < 49) {
						$("#header").removeClass("fixed");
						$("#header").css("margin-top","50px");
						$("#header").css("background","transparent");
						$("#header").css("box-shadow","none");
						$(".show-on-scroll").css("margin-top","-113px");
						$("#menu").css("color","#fff");
						$("#search").css("color","#fff");
					  	$("#logo").html('<img src="<?php echo $base_url ?>images/bloglogo.png" alt="Logo"/>')				 
					}
				}); 

			}	
			
		
		
		//slide in Home blog post
		$(window).scroll(function () {
			//if you hard code, then use console
			//.log to determine when you want the 
			//nav bar to stick.  
			if ($(window).scrollTop() > 1820) {
				$(".blog-itemGroup").css("left","0");
			}
		});
	</script>
</head>
<body id="top">
	<!--
		=======================
			HeaderTop
		=======================
	-->
	<div id="headerTop">
		<div class="headerItems-group topGroup clearfix">
			<div class="topBox altblog">
				<div class="topItem">
					<span class="topIcons topItem-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
					<a href="<?php echo $base_url ?>contact#map" class="all-links topItem-link">Feo Oyeo Link, North Industrial Area, Accra-Ghana</a>
				</div>
			</div>
			<div class="topBox altblog">
				<div class="topItem altblog">
					<span class="topIcons topItem-icon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
					Monday - Friday: 08:00am - 05:00pm
				</div>
			</div>
			<div class="topBox altblog">
				<div class="topItem">
					<span class="topIcons topItem-icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
					<a href="tel:+233 30 226 5323" class="all-links">+233 30 226 5323</a> / <a href="tel:+233 20 636 7332" class="all-links">+233 20 636 7332</a>
				</div>
			</div>
		</div>
	</div>
	<!--
		=======================
			HeaderTop End
		=======================
	-->
	<!--
		=======================
			Header
		=======================
	-->
	<div id="header">
		<div class="headerItems-group clearfix">
			<div id="logo">
				<a href="<?php echo $base_url ?>"><img src="<?php echo $base_url ?>images/bloglogo.png" alt="Logo"/></a>
			</div>
			<div class="show-on-scroll clearfix">
				<div class="topBox">
					<div class="topItem">
						<span class="topIcons topItem-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
						<a href="<?php echo $base_url ?>contact#map" class="all-links topItem-link">Feo Oyeo Link, North Industrial Area, Accra-Ghana</a>
					</div>
				</div>
				<div class="topBox">
					<div class="topItem">
						<span class="topIcons topItem-icon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
						Monday - Friday: 08:00am - 05:00pm
					</div>
				</div>
				<div class="topBox">
					<div class="topItem">
						<span class="topIcons topItem-icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
						<a href="tel:+233 30 226 5323" class="all-links">+233 30 226 5323</a> / <a href="tel:+233 20 636 7332" class="all-links">+233 20 636 7332</a>
					</div>
				</div>
			</div>
			<div id="search" class="altblog">
				<span class="openSearch"><i class="fa fa-search" aria-hidden="true"></i></span>
			</div>
			<div id="menu" class="openMenu altblog clearfix">
				<span class="hide-mo">MENU</span>
				<span><i class="fa fa-bars" aria-hidden="true"></i></span>
			</div>
			<div class="popMenu fixed">
				<span class="close-popMenu">x</span>
				<ul class="nav">
					<li class="nav-item"><a href="<?php echo $base_url ?>" class="all-links current-item">Home</a></li>
					<li class="nav-item"><a href="<?php echo $base_url ?>about" class="all-links">About</a></li>
					<li class="nav-item"><a href="<?php echo $base_url ?>services" class="all-links">Services</a></li>
					<li class="nav-item"><a href="<?php echo $base_url ?>team" class="all-links">Team</a></li>
					<li class="nav-item"><a href="<?php echo $base_url ?>contact" class="all-links">Contact</a></li>
					<li class="nav-item"><a href="<?php echo $base_url ?>clients" class="all-links">Clients</a></li>
				</ul>
			</div>
			<div class="popSearch fixed">
				<span class="close-popSearch">x</span>
				<form id="searchForm" method="POST" action="search.php">
					<input autocomplete="off" type="text" class="searchInput" name="searchInput" placeholder="Search site..." autofocus="autofocus"/>
				</form>
				<ul class="searchResults">
				</ul>
			</div>
		</div>
	</div>
	<!--
		=======================
			Header End
		=======================
	-->