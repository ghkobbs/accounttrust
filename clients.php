<?php
	ob_start();
	$pagetitle = "AccounTrust Ghana Limited | Accounting Company In Accra, Ghana | Home";
	include("header.php");

	
	if(isset($_POST['submit'])){
		$name = mysqli_real_escape_string($db_connect, $_POST['name']);
		$email = mysqli_real_escape_string($db_connect, $_POST['email']);
		$phone = mysqli_real_escape_string($db_connect, $_POST['phone']);
		$subject = mysqli_real_escape_string($db_connect, $_POST['subject']);
		$message = mysqli_real_escape_string($db_connect, $_POST['message']);

		if($message != ""){
		
			$insmsg = mysqli_query($db_connect, "INSERT INTO `messages` (`id`, `name`, `email`, `phone`, `subject`, `message`) VALUES (NULL, '$name', '$email', '$phone', '$subject', '$message') ");
		}
		ob_end_clean();

		if($insmsg){
			require 'phpmailer/PHPMailerAutoload.php';

			$mail = new PHPMailer;

			//$mail->SMTPDebug = 3;                               // Enable verbose debug output

			$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = 'bluewavestechsmtp@gmail.com';                 // SMTP username
			$mail->Password = 'demonstration2016';                           // SMTP password
			$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 587;                                    // TCP port to connect to

			$mail->setFrom($email, $name);
			$mail->addAddress('mmorrison@webteklimited.com', 'Maxwell Morrison');     // Add a recipient
			$mail->addReplyTo($email, $name);
			//$mail->addCC('cc@example.com');
			//$mail->addBCC('bcc@example.com');
			$mail->isHTML(true);                                  // Set email format to HTML

			$mail->Subject = "hello";
			$mail->Body    = '<html><body>';
			$mail->Body    .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
			$mail->Body    .= "<tr><td><strong>Name:</strong> </td><td>" . $name . "</td></tr>";
			$mail->Body    .= "<tr><td><strong>Email:</strong> </td><td>" .$email . "</td></tr>";
			$mail->Body    .= "<tr><td><strong>Contact:</strong> </td><td>" .$phone . "</td></tr>";
			$mail->Body    .= "<tr><td><strong>Message:</strong> </td><td>" .$message . "</td></tr>";
			$mail->Body    .= "</table>";
			$mail->Body    .= '</body></html>';
			$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			if(!$mail->send()) {
			    echo json_encode(array("messageResponse" => $mail->ErrorInfo));
				exit();
			} else {
			    echo json_encode(array("messageResponse" => "added"));
				exit();
			}
		}
	}	

?>
	<!--
		=======================
			staticImage
		=======================
	-->
	<div id="staticImage">
		<img src="<?php echo $base_url ?>images/s-about.jpg" alt="About Main Image">
		<div class="staticText clearfix">
			Our Clients
			<ul class="breadcrumbs">
				<li><a href="<?php echo $base_url ?>" class="breadcrumbs-links">Accountrust</a></li>
					<span class="breadcrumbs-div"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
				<li>Contact</li>
			</ul>
		</div>
		<div id="slider-overlay"></div>
		<div class="url"><?php echo $base_url ?></div>
	</div>
	<!--
		=======================
			staticImage End
		=======================
	-->
	<!--
		=======================
			Clients
		=======================
	-->
	<div id="clientPage">
		<div class="clientsGroup clearfix">
			<div class="clientItem">
				<img src="<?php echo $base_url ?>images/clients/client1.png" alt="Client 1">
				<div class="clientInfo">Up the Ants</div>
			</div>
			<div class="clientItem">
				<img src="<?php echo $base_url ?>images/clients/client2.png" alt="Client 1">
				<div class="clientInfo">Summit Investment</div>
			</div>
			<div class="clientItem">
				<img src="<?php echo $base_url ?>images/clients/client3.png" alt="Client 1">
				<div class="clientInfo">Kiros</div>
			</div>
			<div class="clientItem">
				<img src="<?php echo $base_url ?>images/clients/client4.png" alt="Client 1">
				<div class="clientInfo">Comminuty</div>
			</div>
			<div class="clientItem">
				<img src="<?php echo $base_url ?>images/clients/client5.png" alt="Client 1">
				<div class="clientInfo">Growup Logistics</div>
			</div>
			<div class="clientItem">
				<img src="<?php echo $base_url ?>images/clients/client6.png" alt="Client 1">
				<div class="clientInfo">Prism Collective Design</div>
			</div>
		</div>
	<!--
		=======================
			Clients
		=======================
	-->
<?php include("footer.php") ?>