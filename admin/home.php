<?php 
	ob_start();
	include("../db_connect.php");

	session_start();
	if (isset($_SESSION['username'])){

		$username = $_SESSION['username'];

	}
	else {
		
		$username = '';
	}

	if(!$username){

		header("location: index.php?error=sign");

	} else {

	}

	if(isset($_POST['submit'])){

			$blog_image = mysqli_real_escape_string($db_connect, $_POST['blog_image']);
			$blog_title = mysqli_real_escape_string($db_connect, $_POST['blog_title']);
			$blog_url = mysqli_real_escape_string($db_connect, $_POST['blog_url']);
			$blog_content = mysqli_real_escape_string($db_connect, $_POST['blog_content']);

			$blog_date = date('Y-m-d H:i:s');

			$insblog = mysqli_query($db_connect, "INSERT INTO blog (`blog_id`, `blog_title`, `blog_url`, `blog_image`, `blog_date`, `blog_content`, `blog_authour`) VALUES (NULL, '$blog_title', '$blog_url', '$blog_image', '$blog_date', '$blog_content', '$username') ");
			$inssqlcount = mysqli_num_rows($sql);

			ob_end_clean();
			if ($insblog){
				echo json_encode(array("response"=>"Posted"));
				exit();

			} else {
				echo json_encode(array("response"=>"notPosted"));
				exit();
			}
	}

?>
<html>
<head>
	<title>Accountrust Administrator</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="AccounTrust Ghana Limited is an Accounting Company In Accra, Ghana. Looking for a full Accounting Company Company? You came to the right place &ndash; learn more now!">
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta http-equiv="etag" content="5fc27cec8b90748f50dc1de63810f9e9"/>
	<link rel="publisher" href="https://plus.google.com/+Accountrustltd"/>
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>admin/css/style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>css/font-awesome.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>css/font-awesome.min.css" />
	<script type="text/javascript" src="<?php echo $base_url ?>js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo $base_url ?>admin/js/tinymce.min.js"></script>
    <script type="text/javascript" src="<?php echo $base_url ?>admin/js/jquery.tinymce.min.js"></script>
    <script type="text/javascript" src="<?php echo $base_url ?>admin/js/custom-tinymce.js"></script>
</head>
<body>
	<div class="container">
		<div class="header clearfix">
			<ul class="nav clearfix">
				<li class="nav-item current"><a href="" class="all-link" >Post Blog</a></li>
				<li class="nav-item"><a href="" class="all-link" >Edit Blog</a></li>
				<li class="nav-item"><a href="logout.php" class="all-link" >Log out</a></li>
			</ul>
		</div>
		<div class="blogForm">
			<div class="successBox"></div>
			<div class="errorBox"></div>
			<form id="addBlog" class="clearfix" method="post" action="" enctype="multipart/form-data">
				<div class="input-box input-small clearfix">
					<input type="file" class="blogImage inputField" name="blog_image"  >
					<div class="uploadImage">upload</div>
					<div class="duploadImage">upload</div>
					<div class="error imageerror"></div>
					<div class="imageHolder"></div>
				</div>
				<div class="input-box input-small clearfix">
					<input type="text" class="blogTitle inputField" name="blog_title" placeholder="Blog Title *"  >
					<div class="error titleerror"></div>
				</div>
				<div class="input-box input-small clearfix">
					<input type="text" class="blogUrl inputField" name="blog_url" placeholder="Blog Url"  >
					<div class="opts">Leave it blank if you want to use blog title as url.</div>
					<div class="error urlerror"></div>
				</div>
				<div class="input-box">
					<textarea class="blogContent inputField-large" name="blog_content" placeholder="Blog Content *" ></textarea>
					<div class="error contenterror"></div>
				</div>
				<button type="submit" class="btn submit-btn" name="submit"><span class="sendSpin"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i></span>Submit</button>
			</form>
		</div>
	</div>

<script type="text/javascript" src="<?php echo $base_url ?>admin/js/global.js"></script>
</body>
</html>