$("#SignupForm").submit(function(event){
	event.preventDefault();

	var firstnameerror;
	var lastnameerror;
	var usernameerror;
	var passworderror;
	var password2error;

	if($(".firstname").val() == ""){
		firstnameerror = "Please enter your firstname";
	} else {
		firstnameerror = "";
	}

	if($.isNumeric($(".firstname").val())){
		firstnameerror = "This Field cannot contain Numbers";
	}

	if($(".lastname").val() == ""){
		lastnameerror = "Please enter your firstname";
	} else {
		lastnameerror = "";
	}

	if($.isNumeric($(".lastname").val())){
		lastnameerror = "This Field cannot contain Numbers";
	}

	if($(".username").val() == ""){
		usernameerror = "Please enter your username";
	} else {
		usernameerror = "";
	}

	if($(".username").val() != "" && $(".username").val().length < 6){
		usernameerror = "Username should at least be 6 characters";
	}

	var passreg = /^[0-9a-zA-Z]+$/;
	var passreg2 = /^[0-9]+$/;
	var passreg3 = /^[a-zA-Z]+$/;


	if($(".password").val().match(passreg2) ){
		passworderror = "Password must contain at least a number or letter";
	} else if($(".password").val().match(passreg3) ){
		passworderror = "Password must contain at least a number or letter";
	} else if($(".password").val() == ""){
		passworderror ="Please enter your password";
	} else {
		passworderror = "";
	}

	
	if($(".password").val() != "" && $(".password").val().length < 8){
		passworderror = "Password should at least be 8 characters";
	}

	if($(".password").val() != $(".cpassword").val() ){
		password2error = "Passwords don't match";
	} else {
		password2error = "";
	}

	$(".firstnameerror").html(firstnameerror);
	$(".lastnameerror").html(lastnameerror);
	$(".usernameerror").html(usernameerror);
	$(".passworderror").html(passworderror);
	$(".password2error").html(password2error);

	if(firstnameerror == "" && lastnameerror == "" && usernameerror == "" && passworderror =="" && password2error ==""){
		var response;

	$.ajax({
			type: 'post',
			url: 'signup.php',
			dataType: 'json',
			data:{
				firstname: $(".firstname").val(),
				lastname: $(".lastname").val(),
				username: $(".username").val(),
				password: $(".password").val(),
				submit: 'submit'
			},
			success: function(data){
				response = (data.response);

				if(response == "Success"){
					$(".LogResponse").fadeIn();
					$(".LogResponse").html("Success");
					$(".LogResponse").css("background","#02fb8a");
					$(".LogResponse").css("color","#29820d");

					setTimeout(function() {
						window.location.replace("home.php");;
					}, 3000);

				} else if(response == "Exists"){
					$(".LogResponse").fadeIn();
					$(".LogResponse").css("background","#900404");
					$(".LogResponse").css("color","#ff6666");
					$(".LogResponse").html("Username Already Exists");
				}
			}
		})
	}
})
//Proposal form file upload.
$(".uploadImage").click(function(e){ //on add input button click

    e.preventDefault();
    e.stopImmediatePropagation();

                
    var myFormData = new FormData();
    myFormData.append('blog_image', $(".blogImage").prop('files')[0]);

    var  sel_files = $('.blogImage').val();

    if(sel_files != ""){


        var file_size = $('.blogImage')[0].files[0].size;

        $(".imageerror").html("");

        var ext = $('.blogImage').val().split('.').pop().toLowerCase();

        if ($.inArray(ext, ['png','jpg','jpeg']) != -1){

            if(file_size < 5242880) {                  

                if (sel_files.substring(3,11) == 'fakepath') {

                    sel_files = sel_files.substring(12);
                } // Remove c:\fake at beginning from localhost chrome
             
                    var response2;

                    $.ajax({
                        url: 'post_blog_image.php',
                        type: 'POST',
                        processData: false, // important
                        contentType: false, // important
                        dataType: 'JSON',
                        data: myFormData,
                        success: function(data){
                            response2 = (data.response);


                            $(".imageHolder").append('<div class="viewup-box"><div id="" class="viewimage-details">'+sel_files+'</div><input id="uploadview" value="'+response2+sel_files+'" type="hidden" name="projectimg[]"/><i class="fa fa-times closeinput" aria-hidden="true"></i></div>'); //add input box
                            //$("#skillview", this).val() == add_button.html();
                            $(".blogImage").val("");
                            $('.uploadImage').css("display","none");
                            $('.duploadImage').css("display","block");

                                
                            $(".closeinput").click(function(e){ //user click on remove text
                                e.preventDefault(); $(this).parent('div').remove();
                            $('.uploadImage').css("display","block");
                            $('.duploadImage').css("display","none");
                            })
                        }
                    });     
            } else {
                    $(".imageerror").html("File size should not exceed 5MB");
            }
        } else {
                $(".imageerror").html("Unsupported file type. Files should be either .jpeg, .jpg, .png");
        }
    } else {
        $(".imageerror").html("Please select a file and click upload");
    }            
});

$("#addBlog").submit(function(event){
	event.preventDefault();

	var imageerror;
	var titleerror;
	var urlerror;
	var contenterror;

	if($(".imageHolder").html() == ""){
		imageerror = "Please select an Image for your blog";
	} else {
		imageerror = "";
	}


	if($(".blogTitle").val() == ""){
		titleerror = "Blog title cannot be blank";
	} else {
		titleerror = "";
	}

	if($(".blogContent").val() == ""){
		contenterror = "Please blog content cannot be blank";
	} else {
		contenterror = "";
	}

	$(".imageerror").html(imageerror);
	$(".titleerror").html(titleerror);
	$(".contenterror").html(contenterror);

	if(imageerror == "" && titleerror == "" && contenterror == "" ){
		var response;

	
	$.ajax({
			type: 'post',
			url: 'home.php',
			dataType: 'json',
			data:{
				blog_image: $("#uploadview").val(),
				blog_title: $(".blogTitle").val(),
				blog_url: $(".blogUrl").val(),
				blog_content: $(".blogContent").val(),
				submit: 'submit'
			},
			success: function(data){
				response = (data.response);

				if(response == "Posted"){
					$(".successBox").html("Blog submitted succesfully");
					$(".successBox").css("display","block");
					$(".errorBox").css("display","none");
					$('body, html').animate({scrollTop:$('.successBox').offset().top}, 'slow');
					$(".blogTitle").val("");
					$(".blogUrl").val("");
					$(".blogContent").val("");
				} else if(response == "notPosted"){
					$(".errorBox").html("Blog post unsuccesfully");
					$(".errorBox").css("display","block");
					$(".successBox").css("display","none");
					$('body, html').animate({scrollTop:$('.errorBox').offset().top}, 'slow');
				}
			}
		})
	}
})
$("#logForm").submit(function(event){
	
	event.preventDefault();

	var usernameerror;
	var passworderror;

	if($(".username").val() == ""){
		usernameerror = "Please enter your username";
	} else {
		usernameerror = "";
	}

	if($(".username").val() != "" && $(".username").val().length < 6){
		usernameerror = "Username is less than 6 characters";
	}

	if($(".password").val() == ""){
		passworderror ="Please enter your password";
	} else {
		passworderror = "";
	}

	
	if($(".password").val() != "" && $(".password").val().length < 8){
		passworderror = "Password is less than 8 characters";
	}


	$(".usernameerror").html(usernameerror);
	$(".passworderror").html(passworderror);

	if( usernameerror == "" && passworderror ==""){
		var response;

	$.ajax({
			type: 'post',
			url: 'index.php',
			dataType: 'json',
			data:{
				username: $(".username").val(),
				password: $(".password").val(),
				submit: 'submit'
			},
			success: function(data){
				response = (data.response);

				if(response == "Success"){
					$(".LogResponse").fadeIn();
					$(".LogResponse").html("Success");
					$(".LogResponse").css("background","#02fb8a");
					$(".LogResponse").css("color","#29820d");

					setTimeout(function() {
						window.location.replace("home.php");;
					}, 3000);

				} else if(response == "password"){
					$(".LogResponse").fadeIn();
					$(".LogResponse").css("background","#900404");
					$(".LogResponse").css("color","#ff6666");
					$(".LogResponse").html("Invalid password");

				} else if(response == "username"){
					$(".LogResponse").fadeIn();
					$(".LogResponse").css("background","#900404");
					$(".LogResponse").css("color","#ff6666");
					$(".LogResponse").html("Invalid username");
				}
			}
		})
	}
})
