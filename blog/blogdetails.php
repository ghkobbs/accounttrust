<?php
	$pagetitle = "AccounTrust Ghana Limited | Accounting Company In Accra, Ghana | Home";
	include("../blogheader.php");

	if(isset($_GET['blogtitle'])){
		$getblogs = mysqli_real_escape_string($db_connect, str_replace('-',' ',$_GET['blogtitle']));
			
		$check = mysqli_query($db_connect, "SELECT * FROM blog WHERE blog_title = '$getblogs' || blog_url = '$getblogs' ");
		if (mysqli_num_rows($check) >=1) {
			$get = mysqli_fetch_assoc($check);
			$blog_id = $get['blog_id'];
			$blog_image = $get['blog_image'];
			$blog_title = $get['blog_title'];
			$blog_url = $get['blog_url'];
			$blog_date = $get['blog_date'];
			$blog_authour = $get['blog_authour'];
			$blog_content = $get['blog_content'];

			$blog_date = date("jS M, Y / H:i:a", strtotime($blog_date));
			$blog_authour_url = str_replace(' ','-',$blog_authour);

			if($blog_url == ""){

				$blog_url = str_replace(' ','-',$blog_title);

			} else {
				$blog_url = str_replace(' ','-',$blog_url);
			}

		} else {
			//echo '<meta http-equiv=\"refresh\" content= \"0; url=http://".$base_url."categories\">';
			
		}
	
	}
?>	

	<!--
		=======================
			BlogPage
		=======================
	-->
	<div class="url"><?php echo $base_url ?></div>
	<div id="blogPage">
		<div class="blogsGroup">
			<div id="blogs">
				<div class="container">
					<div class="blogPosts clearfix">
						<ul class="blog-breadcrumbs">
							<li><a href="<?php echo $base_url ?>" class="blog-breadcrumbs-links">Accountrust</a></li>
								<span class="blog-breadcrumbs-div"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
							<li><a href="<?php echo $base_url ?>blog" class="blog-breadcrumbs-links">Blog</a></li>
								<span class="blog-breadcrumbs-div"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
							<li><?php echo $blog_title ?></li>
						</ul>
						<div class="blogpost">
							<div class="bloglist-group clearfix">
								<?php 

										echo '


											<div class="bloglist-item">
												<div class="bloglist-Title"><h2><a href="" class="blog-links">'.$blog_title.'</a></h2></div>
												<div class="bloglist-Info clearfix">
													<div class="bloglist-Info-head"><span class="bloglist-Info-title">By: </span>'.$blog_authour.'</div>
													<div class="bloglist-Info-head"><span class="bloglist-Info-title">Posted on: </span>'.$blog_date.'</div>
												</div>
												<div class="bloglist-Image">
													<img src="'.$base_url.'images/blog/'.$blog_image.'" />
												</div>
												<ul class="blogShare clearfix">
													<li class="shareList shareHead">Share:</li>
													<li class="shareList fcolor"><a href="https://www.facebook.com/sharer/sharer.php?u='.$base_url.'blog/article/'.$blog_url.'" target="_blank" title="share on facebook" class="share-links"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
													<li class="shareList tcolor"><a href="https://twitter.com/home?status='.$base_url.'blog/article/'.$blog_url.'" target="_blank" title="share on twitter" class="share-links"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
													<li class="shareList lcolor"><a href="https://www.linkedin.com/shareArticle?mini=true&url='.$base_url.'blog/article/'.$blog_url.'&title='.$blog_title.'&summary=&source=" target="_blank" title="share on linkedin" class="share-links"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
													<li class="shareList gcolor"><a href="https://plus.google.com/share?url='.$base_url.'blog/article/'.$blog_url.'" target="_blank" title="share on googleplus" class="share-links"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
												</ul>
												<div class="bloglist-Content">'.$blog_content.'</div>
												<ul class="blogShare clearfix">
													<li class="shareList shareHead">Share:</li>
													<li class="shareList fcolor"><a href="https://www.facebook.com/sharer/sharer.php?u='.$base_url.'blog/article/'.$blog_url.'" target="_blank" title="share on facebook" class="share-links"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
													<li class="shareList tcolor"><a href="https://twitter.com/home?status='.$base_url.'blog/article/'.$blog_url.'" target="_blank" title="share on twitter" class="share-links"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
													<li class="shareList lcolor"><a href="https://www.linkedin.com/shareArticle?mini=true&url='.$base_url.'blog/article/'.$blog_url.'&title='.$blog_title.'&summary=&source=" target="_blank" title="share on linkedin" class="share-links"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
													<li class="shareList gcolor"><a href="https://plus.google.com/share?url='.$base_url.'blog/article/'.$blog_url.'" target="_blank" title="share on googleplus" class="share-links"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
												</ul>


										';

										$page_url = $base_url."blog/article/".$blog_url."?id=".$blog_id;
								?>

									<div class="comments">
										<div id="disqus_thread"></div>
											<script>

											/**
											*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
											*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
											
											var disqus_config = function () {
											this.page.url = "<?php echo $page_url ?>";  // Replace PAGE_URL with your page's canonical URL variable
											this.page.identifier = "<?php echo $page_url ?>" // Replace PAGE_IDENTIFIER with your page's unique identifier variable
											};
											
											(function() { // DON'T EDIT BELOW THIS LINE
											var d = document, s = d.createElement('script');
											s.src = '//accountrust.disqus.com/embed.js';
											s.setAttribute('data-timestamp', +new Date());
											(d.head || d.body).appendChild(s);
											})();
											</script>
											<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
									</div>
								</div>
							</div>
						</div>
						<div class="blog-sidebar">
							<div class="sideHeader">Recent Posts</div>
							<?php 
								$check = mysqli_query($db_connect, "SELECT * FROM blog ORDER BY RAND() DESC LIMIT 4 ");
								while($get = mysqli_fetch_assoc($check)){
									$blog_image = $get['blog_image'];
									$blog_title = $get['blog_title'];
									$blog_url = $get['blog_url'];
									$blog_date = $get['blog_date'];
									$blog_authour = $get['blog_authour'];
									$blog_content = $get['blog_content'];

									$blog_date = date("jS M, Y / H:i:a", strtotime($blog_date));
									$blog_authour_url = str_replace(' ','-',$blog_authour);

									if($blog_url == ""){

										$blog_url = str_replace(' ','-',$blog_title);

									} else {
										$blog_url = str_replace(' ','-',$blog_url);
									}

									echo '


										<div class="sideblogList">
											<div class="sBlog-image">
												<img src="'.$base_url.'images/blog/'.$blog_image.'" />
												<div class="sBlog-title"><a href="'.$base_url.'blog/article/'.$blog_url.'" class="sblog-link">'.$blog_title.'</a></div>
											</div>
											<div class="sBlog-content">'.$blog_content.'</div>
											<a href="'.$base_url.'blog/article/'.$blog_url.'" class="s-blog-link">continue reading →</a>
										</div>


									';

								}

							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--
		=======================
			BlogPage
		=======================
	-->
		<script>

				//Search for blogs within database
		$(".searchInput").on('keyup',function(){

			if($(".searchInput").val() == ""){
				$(".searchInput").val("");
			}

			var post_url = "../../search.php";

			$.ajax({
						type: 'post',
						url: post_url,
						data:{
							searchval: $(".searchInput").val(),
							search: "search"
						},
						success: function(data){
							$(".searchResults").html(data);
						}
					})
		})


	</script>
<script id="dsq-count-scr" src="//accountrust.disqus.com/count.js" async></script>
<?php include("../blogfooter.php") ?>