<?php
	ob_start();
	$pagetitle = "AccounTrust Ghana Limited | Accounting Company In Accra, Ghana | Home";
	include("../blogheader.php");

	
	if(isset($_POST['submit'])){
		$name = mysqli_real_escape_string($db_connect, $_POST['name']);
		$email = mysqli_real_escape_string($db_connect, $_POST['email']);
		$phone = mysqli_real_escape_string($db_connect, $_POST['phone']);
		$subject = mysqli_real_escape_string($db_connect, $_POST['subject']);
		$message = mysqli_real_escape_string($db_connect, $_POST['message']);

		if($message != ""){
		
			$insmsg = mysqli_query($db_connect, "INSERT INTO `messages` (`id`, `name`, `email`, `phone`, `subject`, `message`) VALUES (NULL, '$name', '$email', '$phone', '$subject', '$message') ");
		}
		ob_end_clean();

		if($insmsg){
			require 'phpmailer/PHPMailerAutoload.php';

			$mail = new PHPMailer;

			//$mail->SMTPDebug = 3;                               // Enable verbose debug output

			$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = 'bluewavestechsmtp@gmail.com';                 // SMTP username
			$mail->Password = 'demonstration2016';                           // SMTP password
			$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 587;                                    // TCP port to connect to

			$mail->setFrom($email, $name);
			$mail->addAddress('mmorrison@webteklimited.com', 'Maxwell Morrison');     // Add a recipient
			$mail->addReplyTo($email, $name);
			//$mail->addCC('cc@example.com');
			//$mail->addBCC('bcc@example.com');
			$mail->isHTML(true);                                  // Set email format to HTML

			$mail->Subject = "hello";
			$mail->Body    = '<html><body>';
			$mail->Body    .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
			$mail->Body    .= "<tr><td><strong>Name:</strong> </td><td>" . $name . "</td></tr>";
			$mail->Body    .= "<tr><td><strong>Email:</strong> </td><td>" .$email . "</td></tr>";
			$mail->Body    .= "<tr><td><strong>Contact:</strong> </td><td>" .$phone . "</td></tr>";
			$mail->Body    .= "<tr><td><strong>Message:</strong> </td><td>" .$message . "</td></tr>";
			$mail->Body    .= "</table>";
			$mail->Body    .= '</body></html>';
			$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			if(!$mail->send()) {
			    echo json_encode(array("messageResponse" => $mail->ErrorInfo));
				exit();
			} else {
			    echo json_encode(array("messageResponse" => "added"));
				exit();
			}
		}
	}	

?>	

	<!--
		=======================
			BlogPage
		=======================
	-->
	<div class="url"><?php echo $base_url ?></div>
	<div id="blogPage">
		<div class="blogsGroup">
			<div id="blogs">
				<div class="mainTitle"><h1>Welcome to our blog</h1></div>
				<div class="container">
					<div class="blogPosts clearfix">
						<ul class="blog-breadcrumbs">
							<li><a href="<?php echo $base_url ?>" class="blog-breadcrumbs-links">Accountrust</a></li>
								<span class="blog-breadcrumbs-div"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
							<li>Blog</li>
						</ul>
						<div class="bloglist-group clearfix">

							<?php 
								$check = mysqli_query($db_connect, "SELECT * FROM blog ORDER BY blog_id DESC ");
								while($get = mysqli_fetch_assoc($check)){
									$blog_image = $get['blog_image'];
									$blog_title = $get['blog_title'];
									$blog_url = $get['blog_url'];
									$blog_date = $get['blog_date'];
									$blog_authour = $get['blog_authour'];
									$blog_content = $get['blog_content'];

									$blog_date = date("jS M, Y / H:i:a", strtotime($blog_date));
									$blog_authour_url = str_replace(' ','-',$blog_authour);

									if($blog_url == ""){

										$blog_url = str_replace(' ','-',$blog_title);

									} else {
										$blog_url = str_replace(' ','-',$blog_url);
									}

									echo '

										<div class="bloglist-item">
											<div class="bloglist-Title"><h2><a href="'.$base_url.'blog/article/'.$blog_url.'" class="blog-links">'.$blog_title.'</a></h2></div>
											<div class="bloglist-Info clearfix">
												<div class="bloglist-Info-head"><span class="bloglist-Info-title">By: </span>'.$blog_authour.'</div>
												<div class="bloglist-Info-head"><span class="bloglist-Info-title">Posted on: </span>'.$blog_date.'</div>
											</div>
											<div class="bloglist-Image">
												<img src="'.$base_url.'images/blog/'.$blog_image.'" />
											</div>
											<div class="bloglist-Content">'.$blog_content.'</div>
											<a href="'.$base_url.'blog/article/'.$blog_url.'" class="btn blog-btn main-btn">Read more</a>
										</div>


									';

								}

							?>
						</div>
						<div class="blog-sidebar">
							<div class="sideHeader">Recent Posts</div>
							<?php 
								$check = mysqli_query($db_connect, "SELECT * FROM blog ORDER BY RAND() DESC LIMIT 4 ");
								while($get = mysqli_fetch_assoc($check)){
									$blog_image = $get['blog_image'];
									$blog_title = $get['blog_title'];
									$blog_url = $get['blog_url'];
									$blog_date = $get['blog_date'];
									$blog_authour = $get['blog_authour'];
									$blog_content = $get['blog_content'];

									$blog_date = date("jS M, Y / H:i:a", strtotime($blog_date));
									$blog_authour_url = str_replace(' ','-',$blog_authour);

									if($blog_url == ""){

										$blog_url = str_replace(' ','-',$blog_title);

									} else {
										$blog_url = str_replace(' ','-',$blog_url);
									}

									echo '


										<div class="sideblogList">
											<div class="sBlog-image">
												<img src="'.$base_url.'images/blog/'.$blog_image.'" />
												<div class="sBlog-title"><a href="'.$base_url.'blog/article/'.$blog_url.'" class="sblog-link">'.$blog_title.'</a></div>
											</div>
											<div class="sBlog-content">'.$blog_content.'</div>
											<a href="'.$base_url.'blog/article/'.$blog_url.'" class="s-blog-link">continue reading →</a>
										</div>


									';

								}

							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--
		=======================
			BlogPage
		=======================
	-->
		<script>

				//Search for blogs within database
		$(".searchInput").on('keyup',function(){

			if($(".searchInput").val() == ""){
				$(".searchInput").val("");
			}

			var post_url = "../search.php";

			$.ajax({
						type: 'post',
						url: post_url,
						data:{
							searchval: $(".searchInput").val(),
							search: "search"
						},
						success: function(data){
							$(".searchResults").html(data);
						}
					})
		})


	</script>
<?php include("../blogfooter.php") ?>