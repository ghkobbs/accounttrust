//Search for blogs within database
$(".searchInput").on('keyup',function(){

	if($(".searchInput").val() == ""){
		$(".searchInput").val("");
	}

	var post_url = "search.php";

	$.ajax({
				type: 'post',
				url: post_url,
				data:{
					searchval: $(".searchInput").val(),
					search: "search"
				},
				success: function(data){
					$(".searchResults").html(data);
				}
			})
})
