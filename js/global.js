$(document).ready(function(){

	//PopMenu 
	$(".popMenu").hide();

	$(".openMenu").click(function(){
		$(".popMenu").show();
		//$("#top").addClass("fixed");
	})
	$(".close-popMenu").click(function(){
		$(".popMenu").fadeOut();
		//$("#top").removeClass("fixed");
	})

	//PopSearch 
	$(".popSearch").hide();

	$(".openSearch").click(function(){
		$(".popSearch").show();
		//$("#top").addClass("fixed");
	})
	$(".close-popSearch").click(function(){
		$(".popSearch").fadeOut();
		$(".popMenu").fadeOut();
		//$("#top").removeClass("fixed");
	})
})

$(document).ready(function(){
	//Make navigation fixed
	$(window).scroll(function () {
		//if you hard code, then use console
		//.log to determine when you want the 
		//nav bar to stick.  
		if ($(window).scrollTop() > 200) {
			$(".scrolltop").css("display","block");
		}
		if ($(window).scrollTop() < 200) {
			$(".scrolltop").css("display","none");			 
		}
	});

	$(".scrolltop").click(function(){
		$('body, html').animate({scrollTop:$('#top').offset().top}, 'slow')
	})
});

//Calender Click script
$('.divcalendar').on('click', '.availDay', function (){
    var choDate = $(this).html();
       
    $(".selDate").html(choDate)
    $(".aptDate").css("display","block");

});

$(".aptReset").click(function(){
	$(".selDate").html("click on a date from the calender");
    $(".aptDate").css("display","none");
})

//Appointment scheduling
$(".step1").click(function(e){
	e.preventDefault();

	var aptnameerror;
	var aptemailerror;
	var aptphoneerror;
	var aptmessageerror;
	var aptdateerror;

		function isValidEmailAddress(emailAddress) {
    	var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    	return pattern.test(emailAddress);
	};

	//Check if name field is empty
	if($(".name").val() == ""){
		aptnameerror = "Please enter your name";
	} else {
		aptnameerror = "";
	}

	//Check if name field contains numbers
	if($.isNumeric($(".name").val())){
		aptnameerror = "Only letters are allowed";
	}

	//Check if email is valid
	if(!isValidEmailAddress($(".email").val())){
		aptemailerror = "Please enter a valid email address";
	} else {
	aptemailerror = "";
	}

	//Check if phone number is valid
	if(!$.isNumeric($(".phone").val()) && $(".phone").val() != "" || $(".phone").val() == ""){
		aptphoneerror = "Please enter a valid phone number";
	} else {
		aptphoneerror = "";
	}

	//Check if name field is empty
	if($(".message").val() == ""){
		aptmessageerror = "Please enter your message";
	} else {
		aptmessageerror = "";
	}

	//Check if name field is empty
	if($(".selDate").html() == "" || $(".selDate").html() == "click on a date from the calender"){
		aptdateerror = "Appointment date not selected";
	} else {
		aptdateerror = "";
	}

	$(".aptnameerror").html(aptnameerror);
	$(".aptemailerror").html(aptemailerror);
	$(".aptphoneerror").html(aptphoneerror);
	$(".aptmessageerror").html(aptmessageerror);
	$(".aptdateerror").html(aptdateerror);

	if(aptnameerror == "" && aptemailerror =="" && aptphoneerror=="" && aptmessageerror=="" && aptdateerror==""){
		$("#step1").fadeOut();
		$("#step2").fadeIn();

		var detname = $(".name").val();
		var detemail = $(".email").val();
		var detphone = $(".phone").val();
		var detcompany = $(".company").val();
		var detmessage = $(".message").val();
		var detdate = $(".selDate").html() + " " + $(".monthandyearspan").html();

		$(".det-date").html(detdate);
		$(".det-name").html(detname);
		$(".det-email").html(detemail);
		$(".det-phone").html(detphone);
		$(".det-company").html(detcompany);
		$(".det-message").html(detmessage);

		setTimeout(function(){
			$("#step2").fadeOut();
			$("#step3").fadeIn();
		}, 10000)
	}

})
$("#appointmentForm").submit(function(event){
	event.preventDefault();

		var detdate = $(".det-date").html();
		var detname = $(".det-name").html();
		var detemail = $(".det-email").html();
		var detphone = $(".det-phone").html();
		var detcompany = $(".det-company").html();
		var detmessage = $(".det-message").html();

	if(detdate =="" || detname == "" || detemail =="" || detphone == "" || detmessage =="" ){
		uerror = "There was an error while trying to schedule your appointment. Please try again.";
		$(".uerror").addClass("sh-uerror");
	} else {
		uerror = "";
		$(".uerror").removeClass("sh-uerror");
	}

	$(".uerror").html(uerror);	


	if(uerror == ""){
		var response;
		var exits;
		var nameexists;
		var emailexists;
		var phoneexists;
		var companyexists;
		var messageexists;
		var dateexists;


		$("#step2").fadeIn();
		$("#step3").fadeOut();

		$.ajax({
			type: 'post',
			url: 'appointment.php',
			dataType: 'json',
			data:{
				aptdate: detdate,
				aptname: detname,
				aptemail: detemail,
				aptphone: detphone,
				aptcompany: detcompany,
				aptmessage: detmessage,
				submit: "submit"
			},
			success: function(data){
				response = (data.messageResponse);

				exists = (data.dateExit);
				nameexists = (data.name1);
				emailexists = (data.email);
				phoneexists = (data.phone);
				companyexists = (data.company);
				messageexists = (data.message);
				dateexists = (data.date);

				if(response == "booked"){
					$("#step2").fadeOut();
					$(".aptsuccess").html("Thank you! Your appointment has been scheduled succefully. A copy of your appointment details has been sent to your email.");
					$(".aptsuccess").addClass("sh-aptsuccess");
						
				} else if(exists == "dateExit") {
					$("#step3").hide();
					$("#step2").hide();
					$("#step1").fadeIn();
			
					$(".name").val(nameexists);
					console.log(nameexists);
					$(".email").val(emailexists);
					console.log(emailexists);
					$(".phone").val(phoneexists);
					console.log(phoneexists);
					$(".comapny").val(companyexists);
					console.log(companyexists);
					$(".message").val(messageexists);
					console.log(messageexists);
					$(".selDate").html(dateexists);
					console.log(dateexists);
					$(".nerror").html("There are no available slot for selected date. Please select a new date ");
					$(".nerror").addClass("sh-uerror");
					$(".aptDate").css("display","none");
				} else if (response != "booked"){
					$(".uerror").html("There was an error while trying to schedule your appointment. Please refresh the page and try again.");
					$(".uerror").addClass("sh-uerror");
				}
			}
		})

	}

})
$(".bookapt").click(function(event){
	event.preventDefault();

	$("#apt-section").css("right","0");

})

$(".aptclose").click(function(){

	$("#apt-section").css("right","-350px");
	$(".nerror").html("");
	$(".nerror").removeClass("sh-uerror");
	$(".name").val("");
	$(".email").val("");
	$(".phone").val("");
	$(".company").val("");
	$(".message").val("");
	$(".selDate").html("click on a date from the calender");
	$(".aptDate").css("display","none");

});
$("#slider-overlay").click(function(){

	$("#apt-section").css("right","-350px");
	$(".nerror").html("");
	$(".nerror").removeClass("sh-uerror");
	$(".name").val("");
	$(".email").val("");
	$(".phone").val("");
	$(".company").val("");
	$(".message").val("");
	$(".selDate").html("click on a date from the calender");
	$(".aptDate").css("display","none");

});

$(".container").click(function(){

	$("#apt-section").css("right","-350px");
	$(".nerror").html("");
	$(".nerror").removeClass("sh-uerror");
	$(".name").val("");
	$(".email").val("");
	$(".phone").val("");
	$(".company").val("");
	$(".message").val("");
	$(".selDate").html("click on a date from the calender");
	$(".aptDate").css("display","none");

});

//Team Section Profile View
$(".openpro").click(function(event){

	event.preventDefault();

	var dataId = $(this).data("id");
	var image = $("#teamItem-Image"+dataId+" img").attr("src");
	var name = $("#teamItem-name"+dataId).html();
	var title = $("#teamItem-title"+dataId).html();
	var facebook = $("#facebook"+dataId).attr("href");
	var twitter = $("#twitter"+dataId).attr("href");
	var instagram = $("#instagram"+dataId).attr("href");
	var linkedin = $("#linkedin"+dataId).attr("href");
	var bio = $("#Bio"+dataId).html();

	$(".viewImage img").attr("src", image);
	$(".viewName").html(name);
	$(".viewTitle").html(title);
	$(".viewBio").html(bio);
	$("#viewFacebook").attr("href", facebook);
	$("#viewTwitter").attr("href", twitter);
	$("#viewInstagram").attr("href", instagram);
	$("#viewLinkedin").attr("href", linkedin);
	$(".viewPro").css("right","0");

})

$(".closeView").click(function(){

	$(".viewImage img").attr("src", "");
	$(".viewName").html("");
	$(".viewTitle").html("");
	$(".viewBio").html("");
	$("#viewFacebook").attr("href", "");
	$("#viewTwitter").attr("href", "");
	$("#viewInstagram").attr("href", "");
	$("#viewLinkedin").attr("href", "");
	$(".viewPro").css("right","-400px");
})