
	var nameerror = "",
		emailerror = "",
		phoneerror = "",
		messageerror = "";

	function isValidEmailAddress(emailAddress) {
    	var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    	return pattern.test(emailAddress);
	};

	$("#contactForm").submit(function(event){
		event.preventDefault();

		//Check if name field is empty
		if($(".name").val() == ""){
			nameerror = "Please enter your name";
		} else {
			nameerror = "";
		}

		//Check if name field contains numbers
		if($.isNumeric($(".name").val())){
			nameerror = "Only letters are allowed";
		}

		//Check if email is valid
		if(!isValidEmailAddress($(".email").val())){
			emailerror = "Please enter a valid email address";
		} else {
			emailerror = "";
		}
		//Check if phone number is valid
		if(!$.isNumeric($(".phone").val()) && $(".phone").val() != ""){
			phoneerror = "Please enter a valid phone number";
		} else {
			phoneerror = "";
		}

		//Check if name field is empty
		if($(".message").val() == ""){
			messageerror = "Please enter your message";
		} else {
			messageerror = "";
		}


		$(".nameerror").html(nameerror);
		$(".emailerror").html(emailerror);
		$(".phoneerror").html(phoneerror);
		$(".messageerror").html(messageerror);

		if(nameerror== "" && emailerror == "" && phoneerror =="" && messageerror ==""){
			var response;
			$(".sendSpin").css("display","inline-block");

			$.ajax({
				type: 'post',
				url: 'contact.php',
				dataType: 'json',
				data:{
					name: $(".name").val(),
					email: $(".email").val(),
					phone: $(".phone").val(),
					subject: $(".subject").val(),
					message: $(".message").val(),
					submit: "submit"
				},
				success: function(data){
					response = (data.messageResponse);
					if(response == "added"){
						$(".sendSpin").css("display","none");
						$(".mailSuccess").css("display","block");
						$(".mailSuccess").html("Thank you! Your message have been received.");
                      	$('body, html').animate({scrollTop:$('#top').offset().top}, 'slow');
						$(".name").val("");
						$(".email").val("");
						$(".phone").val("");
						$(".subject").val("");
						$(".message").val("");
					} else if(response != "added") {
						$(".sendSpin").css("display","none");
						$(".mailError").css("display","block");
						$(".mailError").html("There was an error while sending your message. Please check back later.");
                      	$('body, html').animate({scrollTop:$('#top').offset().top}, 'slow');
					}
				}
			})
		}


	})


