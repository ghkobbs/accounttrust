<?php
	$pagetitle = "AccounTrust Ghana Limited | Accounting Company In Accra, Ghana | Home";
	include("header.php");

?>
	<!--
		=======================
			staticImage
		=======================
	-->
	<div id="staticImage">
		<img src="<?php echo $base_url ?>images/s-about.jpg" alt="About Main Image">
		<div class="staticText clearfix">
			Our Values
			<ul class="breadcrumbs">
				<li><a href="<?php echo $base_url ?>" class="breadcrumbs-links">Accountrust</a></li>
					<span class="breadcrumbs-div"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
				<li>Values</li>
			</ul>
		</div>
		<div id="slider-overlay"></div>
		<div class="url"><?php echo $base_url ?></div>
	</div>
	<!--
		=======================
			staticImage End
		=======================
	-->
	<!--
		=======================
			Values
		=======================
	-->
	<div id="valuesPage">
		<div class="container">
			<div class="values clearfix">
				<div class="valueMain-text">
					Our firm provides outstanding service to our clients because of our dedication to the three underlying principles of professionalism, responsiveness and quality.
				</div>
				<div class="valuesItem clearfix">
					<div class="valuesIcon">
						<img src="<?php echo $base_url ?>images/pro-icon.png" alt="icon">
					</div>
					<div class="valuesInfo-box">
						<div class="valuesName">Professionalism</div>
						<div class="valuesDesc">
							<p class="aboutText">Our firm is one of the leading firms in the area. By combining our expertise, experience and the energy of our staff, each client receives close personal and professional attention.</p>
							<p class="aboutText">Our high standards, service and specialized staff spell the difference between our outstanding performance, and other firms. We make sure that every client is served by the expertise of our whole firm.</p>
						</div>
					</div>
				</div>
				<div class="valuesItem clearfix">
					<div class="valuesIcon">
						<img src="<?php echo $base_url ?>images/res-icon.png" alt="icon">
					</div>
					<div class="valuesInfo-box">
						<div class="valuesName">Responsiveness</div>
						<div class="valuesDesc">
							<p class="aboutText">Our firm is responsive. Companies who choose our firm rely on competent advice and fast, accurate personnel. We provide total financial services to individuals, large and small businesses and other agencies.</p>
							<p class="aboutText">To see a listing of our services, please take a moment and look at our services page. Because we get new business from the people who know us best, client referrals have fueled our growth in the recent years.</p>
							<p class="aboutText">Through hard work, we have earned the respect of the business and financial communities. This respect illustrates our diverse talents, dedication and ability to respond quickly.</p>
						</div>
					</div>
				</div>
				<div class="valuesItem clearfix">
					<div class="valuesIcon">
						<img src="<?php echo $base_url ?>images/qua-icon.png" alt="icon">
					</div>
					<div class="valuesInfo-box">
						<div class="valuesName">Quality</div>
						<div class="valuesDesc">
							<p class="aboutText">An accounting firm is known for the quality of its service. Our firm's reputation reflects the high standards we demand of ourselves.</p>
							<p class="aboutText">Our primary goal as a trusted advisor is to be available to provide insightful advice to enable our clients to make informed financial decisions. We do not accept anything less from ourselves and this is what we deliver to you.</p>
							<p class="aboutText">We feel it is extremely important to continually professionally educate ourselves to improve our technical expertise, financial knowledge and service to our clients.</p>
							<p class="aboutText">Our high service quality and "raving fan" clients are the result of our commitment to excellence.</p>
							<p class="aboutText">We will answer all of your questions, as they impact both your tax and financial situations. We welcome you to contact us anytime.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--
		=======================
			Values End
		=======================
	-->
	<!--
		=======================
			Clients
		=======================
	-->
	<div id="clients">
		<div class="container clearfix">
			<div class="allGroup clientGroup">
				<div class="mainTitle"><h1>Clients</h1></div>
				<div class="clientsItems clearfix">
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client1.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client2.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client3.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client4.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client5.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client6.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client7.png" alt="Clients"></div>
					</div>
					<div class="clientsitem-list">
						<div class="client"><img src="<?php echo $base_url ?>/images/clients/client8.png" alt="Clients"></div>
					</div>
				</div>
				<div class="clientbtn">
					<a href="<?php echo $base_url ?>clients" class="sub-btn main-btn">View more clients</a>
				</div>
			</div>
		</div>
	</div>
	<!--
		=======================
			Clients End
		=======================
	-->
<?php include("footer.php") ?>