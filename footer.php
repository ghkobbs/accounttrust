
<footer class="clearfix">
	<div class="footerItem">
		<div class="footer-logo">
			<img src="<?php echo $base_url ?>images/logo.png" />
		</div>
		<div class="footerAbout">
			Our company is one of the leaders in the market of professional consulting services for medium and large businesses. The peculiarity of our company that distinguishes it from a variety of companies, is the individual integrated approach to solving problems.
		</div>
		<table>
			<tr>
				<td class="footerIcons"><i class="fa fa-map-marker" aria-hidden="true"></i></td>
				<td class="footerTitle"> Feo Oyeo Link, North Industrial Area, Accra-Ghana</td>
			</tr>
			<tr>
				<td class="footerIcons"><i class="fa fa-phone" aria-hidden="true"></i></td>
				<td class="footerTitle"> +233 30 226 5323 / +233 20 636 7332</td>
			</tr>
			<tr>
				<td class="footerIcons"><i class="fa fa-clock-o" aria-hidden="true"></i></td>
				<td class="footerTitle"> Monday - Friday: 08:00am - 05:00pm</td>
			</tr>
			<tr>
				<td class="footerIcons"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></td>
				<td class="footerTitle"><a href="mailto:evelyn@accountrustltd.com" class="footer-links">evelyn@accountrustltd.com</a></td>
			</tr>
		</table>
	</div>
	<div class="footerItem sitemapSec">
		<ul class="footerTitle sitemap clearfix">
			<li class="sitemap-title">Sitemap</li>
			<li class="sitemap-item"><a href="<?php echo $base_url ?>" class="footer-links">Home</a></li>
			<li class="sitemap-item"><a href="<?php echo $base_url ?>about" class="footer-links">About</a></li>
			<li class="sitemap-item"><a href="<?php echo $base_url ?>services" class="footer-links">Services</a></li>
			<li class="sitemap-item"><a href="<?php echo $base_url ?>team" class="footer-links">Team</a></li>
			<li class="sitemap-item"><a href="<?php echo $base_url ?>contact" class="footer-links">Contact</a></li>
			<li class="sitemap-item"><a href="<?php echo $base_url ?>clients" class="footer-links">Clients</a></li>
			<li class="sitemap-item"><a href="<?php echo $base_url ?>values" class="footer-links">Our values</a></li>
			<li class="sitemap-item"><a href="<?php echo $base_url ?>blog" class="footer-links">Blog</a></li>
		</ul>
		<div class="googleReview">
			<a href="https://goo.gl/tUI979" target="_blank"><img src="<?php echo $base_url ?>images/googlereview.png" alt="Write review"></a>
		</div>
	</div>
	<div class="footerItem">
		<div id="footerMap clearfix">
		<iframe class="footer-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3970.9535516043707!2d-0.22650968523399506!3d5.573885995958846!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfdf9a1d66508b9b%3A0xfa2f9b3139f14460!2sAccounTrust+Ghana+Limited!5e0!3m2!1sen!2sgh!4v1484317350852"></iframe>
		<ul class="socialGroup clearfix">
			<li class="socialicons"><a href="http://www.facebook.com/accountrust" target="_blank" class="all-links"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			<li class="socialicons"><a href="http://www.twitter.com/accountrust_" target="_blank" class="all-links"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			<li class="socialicons"><a href="http://www.instagram.com/accountrust" target="_blank" class="all-links"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			<li class="socialicons"><a href="http://www.google.com/+Accountrustltd" target="_blank" class="all-links"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
			<li class="socialicons"><a href="http://www.linkedin.com/company/accountrust-ghana-limited" target="_blank" class="all-links"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
		</ul>
	</div>
	<div class="scrolltop"><i class="fa fa-arrow-up" aria-hidden="true"></i></div>
</footer>
	<div class="footer clearfix">
		<div class="copyright">&copy; 2017 AccounTrust Limited | All Rights Reserved</div>
		<div class="dev">Developed and Powered by <a href="http://www.webteklimited.com/" target="_blank" class="all-links">WEBTEK</a></div>
	</div>
<script type="text/javascript" src="<?php echo $base_url ?>js/global.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>js/searchjs.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>js/validation.js"></script>
</body>
</html>