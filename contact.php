<?php
	ob_start();
	$pagetitle = "AccounTrust Ghana Limited | Accounting Company In Accra, Ghana | Home";
	include("header.php");

	
	if(isset($_POST['submit'])){
		$name = mysqli_real_escape_string($db_connect, $_POST['name']);
		$email = mysqli_real_escape_string($db_connect, $_POST['email']);
		$phone = mysqli_real_escape_string($db_connect, $_POST['phone']);
		$subject = mysqli_real_escape_string($db_connect, $_POST['subject']);
		$message = mysqli_real_escape_string($db_connect, $_POST['message']);

		if($message != ""){
		
			$insmsg = mysqli_query($db_connect, "INSERT INTO `messages` (`id`, `name`, `email`, `phone`, `subject`, `message`) VALUES (NULL, '$name', '$email', '$phone', '$subject', '$message') ");
		}
		ob_end_clean();

		if($insmsg){
			require 'phpmailer/PHPMailerAutoload.php';

			$mail = new PHPMailer;

			//$mail->SMTPDebug = 3;                               // Enable verbose debug output

			$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = 'bluewavestechsmtp@gmail.com';                 // SMTP username
			$mail->Password = 'demonstration2016';                           // SMTP password
			$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 587;                                    // TCP port to connect to

			$mail->setFrom($email, $name);
			$mail->addAddress('mmorrison@webteklimited.com', 'Maxwell Morrison');     // Add a recipient
			$mail->addReplyTo($email, $name);
			//$mail->addCC('cc@example.com');
			//$mail->addBCC('bcc@example.com');
			$mail->isHTML(true);                                  // Set email format to HTML

			$mail->Subject = $subject;
			$mail->Body    = '<html><body>';
			$mail->Body    .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
			$mail->Body    .= "<tr><td><strong>Name:</strong> </td><td>" . $name . "</td></tr>";
			$mail->Body    .= "<tr><td><strong>Email:</strong> </td><td>" .$email . "</td></tr>";
			$mail->Body    .= "<tr><td><strong>Contact:</strong> </td><td>" .$phone . "</td></tr>";
			$mail->Body    .= "<tr><td><strong>Message:</strong> </td><td>" .$message . "</td></tr>";
			$mail->Body    .= "</table>";
			$mail->Body    .= '</body></html>';
			$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			if(!$mail->send()) {
			    echo json_encode(array("messageResponse" => $mail->ErrorInfo));
				exit();
			} else {
			    echo json_encode(array("messageResponse" => "added"));
				exit();
			}
		}
	}	

?>	
	<!--
		=======================
			staticImage
		=======================
	-->
	<div id="staticImage">
		<img src="<?php echo $base_url ?>images/s-about.jpg" alt="About Main Image">
		<div class="staticText clearfix">
			Get in touch
			<ul class="breadcrumbs">
				<li><a href="<?php echo $base_url ?>" class="breadcrumbs-links">Accountrust</a></li>
					<span class="breadcrumbs-div"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
				<li>Contact</li>
			</ul>
		</div>
		<div id="slider-overlay"></div>
		<div class="url"><?php echo $base_url ?></div>
	</div>
	<!--
		=======================
			staticImage End
		=======================
	-->
	<!--
		=======================
			Contact
		=======================
	-->
	<div id="contactPage">
		<div class="container">
			<div class="contact clearfix">
				<div class="contactForm">
					<form id="contactForm" class="clearfix" method="POST" action="">
						<div class="mailSuccess"></div>
						<div class="mailError"></div>
						<div class="input-box input-small clearfix">
							<input type="text" class="name inputField" name="name" placeholder="Name *">
							<div class="error nameerror"></div>
						</div>
						<div class="input-box input-small clearfix">
							<input type="email" class="email inputField" name="email" placeholder="Email *" >
							<div class="error emailerror"></div>
						</div>
						<div class="input-box input-small clearfix">
							<input type="text" class="phone inputField" name="phone" placeholder="Phone"  >
							<div class="error phoneerror"></div>
						</div>
						<div class="input-box input-small clearfix">
							<input type="text" class="subject inputField" name="subject" placeholder="Subject"  >
							<div class="error subjecterror"></div>
						</div>
						<div class="input-box">
							<textarea class="message inputField-large" name="message" placeholder="Message *" ></textarea>
							<div class="error messageerror"></div>
						</div>
						<button type="submit" class="btn submit-btn" name="submit"><span class="sendSpin"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i></span> Send</button>
						<input type="reset" class="btn submit-btn" name="submit" value="reset" >
					</form>
				</div>
			</div>
		</div>
		<div id="map" class="clearfix">
			<div id="mapSection">
			</div>
			<script>

			function initMap() {
		        var map = new google.maps.Map(document.getElementById('mapSection'), {
				    zoom: 15,
				    center: {lat: 5.573872, lng: -0.224336}
		        });

		        var infowindow = new google.maps.InfoWindow();
		        var service = new google.maps.places.PlacesService(map);

		        service.getDetails({
		          placeId: 'ChIJm4tQZh2a3w8RYETxOTGbL_o'
		        }, function(place, status) {
		          if (status === google.maps.places.PlacesServiceStatus.OK) {
		            var marker = new google.maps.Marker({
		              map: map,
		              position: place.geometry.location
		            });

		            var infowindow = new google.maps.InfoWindow();
					infowindow.setContent('<div style="text-align: center"><strong>' + place.name + '</strong><br><br>Feo Oyeo Link, North Industrial Area, Accra-Ghana<br><br>+233 30 226 5323 / +233 20 636 7332</div>');
					infowindow.open(map, marker);
		          }
		        });
		    }
			</script>
		</div>
	</div>
	<!--
		=======================
			Contact
		=======================
	-->

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDYt3mTWj_ujYFgw3mMY5FnUDX762sd95E&callback=initMap&libraries=places"
    async defer></script>
<?php include("footer.php") ?>