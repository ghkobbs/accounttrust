<?php
	ob_start();
	include("db_connect.php");
	require 'phpmailer/PHPMailerAutoload.php';

	
	if(isset($_POST['submit'])){
		$aptdate = mysqli_real_escape_string($db_connect, $_POST['aptdate']);
		$aptname = mysqli_real_escape_string($db_connect, $_POST['aptname']);
		$aptemail = mysqli_real_escape_string($db_connect, $_POST['aptemail']);
		$aptphone = mysqli_real_escape_string($db_connect, $_POST['aptphone']);
		$aptcompany = mysqli_real_escape_string($db_connect, $_POST['aptcompany']);
		$aptmessage = mysqli_real_escape_string($db_connect, $_POST['aptmessage']);

		$chkDate = mysqli_query($db_connect, "SELECT apt_date FROM appointments WHERE apt_date = '$aptdate' ");
		$chkDatecount = mysqli_num_rows($chkDate);


		ob_end_clean();
		if($chkDatecount < 1){
		
			$insapt = mysqli_query($db_connect, "INSERT INTO `appointments` (`apt_id`, `apt_date`, `apt_name`, `apt_email`, `apt_phone`, `apt_company`, `apt_message`) VALUES (NULL, '$aptdate', '$aptname', '$aptemail', '$aptphone', '$aptcompany', '$aptmessage') ");
		} else {
			echo json_encode(array("dateExit" => "dateExit", "name1" => "$aptname", "email" => "$aptemail", "phone" => "$aptphone","company" => "$aptcompany","message" => "$aptmessage", "date" => "$aptdate"));
			exit();
		}
		


		if($insapt){

			$mail = new PHPMailer;

			//$mail->SMTPDebug = 3;                               // Enable verbose debug output

			$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = 'bluewavestechsmtp@gmail.com';                 // SMTP username
			$mail->Password = 'demonstration2016';                           // SMTP password
			$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 587;                                    // TCP port to connect to

			$mail->setFrom($aptemail, $aptname);
			$mail->addAddress('mmorrison@webteklimited.com', 'Maxwell Morrison');     // Add a recipient
			$mail->addReplyTo($aptemail, $aptname);
			//$mail->addCC('cc@example.com');
			//$mail->addBCC('bcc@example.com');
			$mail->isHTML(true);                                  // Set email format to HTML

			$mail->Subject = "Appointment Scheduling";
			$mail->Body    = 'Find details of appointment below:<br /><br />';
			$mail->Body    .= '<br /><strong>Appointment Date</strong><br />';
			$mail->Body    .= $aptdate;
			$mail->Body    .= '<br /><br /><strong>Name </strong><br />';
			$mail->Body    .= $aptname;
			$mail->Body    .= '<br /><br /><strong>Email </strong><br />';
			$mail->Body    .= $aptemail;
			$mail->Body    .= '<br /><br /><strong>Phone Number </strong><br />';
			$mail->Body    .= $aptphone;
			$mail->Body    .= '<br /><br /><strong>Company </strong><br />';
			$mail->Body    .= $aptcompany;
			$mail->Body    .= '<br /><br /><strong>Message </strong><br />';
			$mail->Body    .= $aptmessage;
			$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';


			
			if(!$mail->send()) {
				    echo json_encode(array("messageResponse" => $mail->ErrorInfo));
					exit();
				    
				} else {

					$mail2 = new PHPMailer;

					//$mail->SMTPDebug = 3;                               // Enable verbose debug output

					$mail2->isSMTP();                                      // Set mailer to use SMTP
					$mail2->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
					$mail2->SMTPAuth = true;                               // Enable SMTP authentication
					$mail2->Username = 'bluewavestechsmtp@gmail.com';                 // SMTP username
					$mail2->Password = 'demonstration2016';                           // SMTP password
					$mail2->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
					$mail2->Port = 587;                                    // TCP port to connect to

					$mail2->setFrom('mmorrison@webteklimited.com', 'Maxwell Morrison');
					$mail2->addAddress($aptemail, $aptname);     // Add a recipient
					$mail2->addReplyTo('mmorrison@webteklimited.com', 'Maxwell Morrison');
					//$mail->addCC('cc@example.com');
					//$mail->addBCC('bcc@example.com');
					$mail2->isHTML(true);                                  // Set email2 format to HTML

					$mail2->Subject = 'Appointment Scheduling';
					$mail2->Body    = 'This is an automated email message to confirm that your appointment has been received. <br/><br/>You will receive a final email once your appointment is accepted. <br/><br/> Find below a copy of your appointment details. <br /><hr />';
					$mail2->Body    .= '<br /><strong>Appointment Date</strong><br />';
					$mail2->Body    .= $aptdate;
					$mail2->Body    .= '<br /><br /><strong>Name</strong><br />';
					$mail2->Body    .= $aptname;
					$mail2->Body    .= '<br /><br /><strong>Email</strong><br />';
					$mail2->Body    .= $aptemail;
					$mail2->Body    .= '<br /><br /><strong>Phone Number</strong><br />';
					$mail2->Body    .= $aptphone;
					$mail2->Body    .= '<br /><br /><strong>Company</strong><br />';
					$mail2->Body    .= $aptcompany;
					$mail2->Body    .= '<br /><br /><strong>Message</strong><br />';
					$mail2->Body    .= $aptmessage;
					$mail2->AltBody = 'This is the body in plain text for non-HTML mail clients';


					if($mail2 -> send()){
						echo json_encode(array("messageResponse"=>"booked"));
					}
				}
		}

	}
		

?>	